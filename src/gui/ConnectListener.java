package gui;

import java.awt.event.ActionEvent;
import java.rmi.RemoteException;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import sakk.Babu.Szin;
import sakk.SakkClient;

/**
 * Csatlakozás online játékhoz való listener
 * @author karsazoltan
 *
 */
public class ConnectListener extends JatekListeners {
	public ConnectListener(sakkGUI sgui, TablaPanel p) {
		super(sgui, p);
	}
	/**
	 * Megnyit egy ablakot, ami elkéri a szerver ip-jét, majd csatlakozik hozzá
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		String s = (String) JOptionPane.showInputDialog(jatekgui.jf, "A csatlakozáshoz adja meg a szerver IP-címét!", "Csatlakozás Online játékhoz", JOptionPane.PLAIN_MESSAGE);
		if(s!= null) {
			jatekgui.rstJatek();
			jatekgui.jatek = new SakkClient(s, Szin.fekete);
			if(jatekgui.jatek != null) {
				try {
					tablapanel.ujJatek(jatekgui);
					jatekgui.RemoteActionStart();
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
