package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.time.LocalDate;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;

import sakk.Eredmeny;

/**
 * Eredméynek megjelenítésére alkalmas frame
 * @author karsazoltan
 *
 */
public class EredmenyTableFrame extends JFrame {
	/**
	 * TableModel megvalósítás
	 */
	private EredmenyData data;
	/**
	 * a kereséshez egy szövegbeviteli mező
	 */
	private JTextField keres;
	/**
	 * Alap konstruktor
	 * @param list az eredmények listája
	 */
	public EredmenyTableFrame(List<Eredmeny> list) {
		data = new EredmenyData(list);
		keres = new JTextField(30);
		
		JLabel jl = new JLabel("Játékos: ");
		JButton jb = new JButton("Keres");
		JPanel kerespanel = new JPanel();
		kerespanel.setLayout(new FlowLayout());
		kerespanel.add(jl);
		kerespanel.add(keres);
		kerespanel.add(jb);
		
		setMinimumSize(new Dimension(600, 250));
		setLayout(new BorderLayout());
		JTable jt = new JTable(data);
		jt.setDefaultRenderer(String.class, new EredmenyTableCellRenderer(jt.getDefaultRenderer(String.class)));
        jt.setDefaultRenderer(Boolean.class, new EredmenyTableCellRenderer(jt.getDefaultRenderer(Boolean.class)));
        jt.setDefaultRenderer(LocalDate.class, new EredmenyTableCellRenderer(jt.getDefaultRenderer(LocalDate.class)));
        jt.setRowSorter(new TableRowSorter(jt.getModel()));
        JScrollPane jsp = new JScrollPane(jt);
        jt.setFillsViewportHeight(true);
        
        jb.addActionListener( ae -> jt.updateUI());
        
        this.add(jsp, BorderLayout.CENTER);
        this.add(kerespanel, BorderLayout.SOUTH);
        pack();
        setDefaultCloseOperation(HIDE_ON_CLOSE);
	}
	/**
	 * A táblához tartozó leképező osztály, mely keresés esetén átállítja a sorokat
	 * @author karsazoltan
	 *
	 */
	private class EredmenyTableCellRenderer implements TableCellRenderer {
		private final TableCellRenderer renderer;
		public EredmenyTableCellRenderer(TableCellRenderer defRenderer) 
		{
			this.renderer = defRenderer;
		}
		
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) 
		{
			Component component = renderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			Eredmeny actualStudent = data.eredmenyek.get(table.getRowSorter().convertRowIndexToModel(row));
			String nev = keres.getText();
			Color color = Color.WHITE;
			if(actualStudent.getFeher().compareTo(nev) == 0 || actualStudent.getFekete().compareTo(nev) == 0) {
				if(actualStudent.getNyert().compareTo(nev) == 0)
					color = Color.GREEN;
				else
					color = Color.RED;
			}			
			component.setBackground(color);
			return component;
		}
		
	}

}
