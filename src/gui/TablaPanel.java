package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.rmi.RemoteException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import sakk.IJatek;
import sakk.Babu.Szin;

/**
 * A játék táblának megfelelő JPanel
 * @author karsazoltan
 *
 */
public class TablaPanel extends JPanel {
	/**
	 * A mezőket ezen gombok reprezentálják
	 */
	private MJButton[][] buttons; 
	/**
	 * A bábúk ikonjainak gyűjteménye
	 */
	private ImageIcon[] icons;
	/**
	 * alap konstruktor
	 */
	public TablaPanel() {
		icons = new ImageIcon[12];
		setIcons();
		buttons = new MJButton[8][8];
		this.setMaximumSize(new Dimension(1000, 800));
		this.setMinimumSize(new Dimension(1000, 800));
		this.setPreferredSize(new Dimension(1000, 800));
		ujTabla();
		this.setLayout(new GridLayout(8,8));
	}
	/**
	 * Változást kezelő függvény
	 * @param jatekgui a játék grafikus megvalósítása
	 * @param csere történt-e csere
	 * @throws RemoteException
	 */
	public void valtozas(sakkGUI jatekgui, boolean csere) throws RemoteException {
		int[][] v = jatekgui.jatek.valtozas();
		if(!csere) {
			buttons[v[0][0]][v[0][1]].setIcon(this.getIcon(jatekgui.jatek.getMezo(v[0][0], v[0][1]).getBabu().iconIndex()));
			buttons[v[1][0]][v[1][1]].setIcon(null);
		} else {
			buttons[v[0][0]][v[0][1]].setIcon(this.getIcon(jatekgui.jatek.getMezo(v[0][0], v[0][1]).getBabu().iconIndex()));
		}
	}
	/**
	 * Feltölti a panelt új gombokkal
	 */
	private void ujTabla() {
		for(int j = 0; j < buttons.length; j++) {
			for(int i = 0; i < buttons.length; i++) {
					buttons[j][i] = new MJButton(j, i);
				if((j+i) % 2 == 0)
					buttons[j][i].setBackground(new Color(100, 100, 100));
				else
					buttons[j][i].setBackground(new Color(240, 240, 240));
				this.add(buttons[j][i]);
				buttons[j][i].setSize(100, 100);
			}
		}
	}
	/**
	 * Beállítja a kezelőfüggvényt a mezők számára
	 * @param jatekgui
	 */
	public void setListeners(sakkGUI jatekgui) {
		MezoListener handler = new MezoListener(jatekgui, this, buttons);
		for(int j = 0; j < buttons.length; j++) {
			for(int i = 0; i < buttons.length; i++) {
				buttons[j][i].addActionListener(handler);
			}
		}
	}
	/**
	 * Beállítja az érvényes mezők hátterét
	 * @param pick milyen színű legyen
	 * @throws RemoteException
	 */
	public void setColor(Color pick, IJatek jatek) throws RemoteException {
		for(int j = 0; j < buttons.length; j++) {
			for(int i = 0; i < buttons.length; i++) {
				if(jatek.getMezo(j, i).jo) 
					buttons[j][i].setBackground(pick);
				else if((j+i) % 2 == 0)
					buttons[j][i].setBackground(new Color(100, 100, 100));
				else
					buttons[j][i].setBackground(new Color(240, 240, 240));
			}
		}
	}
	/**
	 * Alapra állítja a mezők színét 
	 */
	private void rstColor() {
		for(int j = 0; j < buttons.length; j++) {
			for(int i = 0; i < buttons.length; i++) {
				if((j+i) % 2 == 0)
					buttons[j][i].setBackground(new Color(100, 100, 100));
				else
					buttons[j][i].setBackground(new Color(240, 240, 240));
			}
		}
	}
	/**
	 * Beállítja a táblát és új Játékot kezd
	 * @param f
	 * @throws RemoteException
	 */
	public void ujJatek(sakkGUI jatekgui) throws RemoteException {
		IJatek jatek = jatekgui.jatek;
		for(int j = 0; j < buttons.length; j++) {
			for(int i = 0; i < buttons.length; i++) {
				if(!jatek.getMezo(j, i).ures()) {
					buttons[j][i].setIcon(icons[jatek.getMezo(j, i).getBabu().iconIndex()]);
				}
				else
					buttons[j][i].setIcon(null);
			}
		}
		setTablaEnabled(true);
		rstColor();
		jatekgui.stopClientAction();
	}
	/**
	 * Törli az ikonokat a tábláról
	 */
	public void clrTable() {
		for(int j = 0; j < buttons.length; j++) {
			for(int i = 0; i < buttons.length; i++) {
				buttons[i][j].setIcon(null);
				if((j+i) % 2 == 0)
					buttons[j][i].setBackground(new Color(100, 100, 100));
				else
					buttons[j][i].setBackground(new Color(240, 240, 240));
			}
		}
	}
	/**
	 * Letiltja/feloldja a gombokat
	 * @param ok
	 */
	public void setTablaEnabled(boolean ok) {
		for(int i = 0; i < buttons.length; i++) {
			for(int j = 0; j < buttons.length; j++)
				buttons[i][j].setEnabled(ok);
		}
	}
	/**
	 * a megadott buttont állatja be a megfelelő icon-ra
	 * @param x
	 * @param y
	 * @param iconIndex
	 */
	public void setButtons(int x, int y, int iconIndex) {
		buttons[x][y].setIcon(icons[iconIndex]);
	}
	/**
	 * Nincs ikon
	 * @param x
	 * @param y
	 */
	public void setButtonsNullIcon(int x, int y) {
		buttons[x][y].setIcon(null);
	}
	/**
	 * Beolvassa a bábuk ikonjait
	 */
	private void setIcons() {
		for(int i = 0; i < icons.length; i++)
			icons[i] = new ImageIcon("pic/" +  (i+1) +".png");
	}
	/**
	 * Visszadja az adott azonosítójú bábúnak megfelelő icon-t
	 * @param i
	 * @return
	 */
	public ImageIcon getIcon(int i) {
		return icons[i];
	}
}
