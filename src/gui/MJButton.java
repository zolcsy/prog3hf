package gui;

import javax.swing.JButton;

/**
 * A sakktábla mezőinek könnyű kezeléséhez, segítségével a cella könnyen beazonosítható
 * @author karsazoltan
 *
 */
public class MJButton extends JButton {
	/**
	 * a mező első koordinátája
	 */
	public int x;
	/**
	 * a mező 2. koordinátája
	 */
	public int y;
	/**
	 * Konstruktor
	 * @param X oszlop azonosító
	 * @param Y sor azonosító
	 */
	public MJButton(int X, int Y) {
		super();
		x = X;
		y = Y;
	}
}
