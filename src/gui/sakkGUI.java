package gui;

import sakk.*;
import java.awt.*;
import java.io.IOException;
import java.rmi.RemoteException;

import javax.swing.*;

import org.jdom2.JDOMException;

/**
 * Megegyező grafikus megvalósítás online és offline módra is
 * @author karsazoltan
 *
 */
public class sakkGUI{
	/**
	 * A grafikához tartozó játék megvalósítás
	 */
	IJatek jatek;
	/**
	 * ha a felhasználó szervert indítana
	 */
	private SakkServer ss;
	/**
	 * a táblához megfelelő panel
	 */
	private TablaPanel tablaPanel;
	/**
	 * maga az ablak
	 */
	JFrame jf;
	//arra az esetre ha online lenne a játék!
	private RemoteActionThread rat;
	/**
	 * Ha a szerver a mi oldalunkon fut
	 * @param s egy szerver példány
	 */
	public void setServer(SakkServer s) {
		ss = s;
	}
	/**
	 * Folyamatason figyeli a távoli szerver változását!
	 */
	public void RemoteActionStart() {
		rat = new RemoteActionThread((SakkClient) jatek, this, tablaPanel);
		rat.start();
	}
	/**
	 * Leállítja a távoli szerver figyelését
	 */
	public void stopClientAction() {
		if(rat != null)
			rat.stopAction();
	}
	/**
	 * Alap konstruktor
	 * @param mb a frame-hez tartozó menü
	 * @param tp a játék sakktáblájának megfelelő grafikus példány
	 */
	public sakkGUI(JMenuBar mb, TablaPanel tp) {
		tablaPanel = tp;
		jf = new JFrame("Játék");
		jf.setMinimumSize(new Dimension(1000, 800));
		jf.setMaximumSize(new Dimension(1000, 1000));
		jf.setLayout(new BorderLayout());
		//JPanel tablaPanel = new JPanel(); // itt lesznek a mezők
		jf.add(mb, BorderLayout.PAGE_START);
		jf.add(tablaPanel, BorderLayout.CENTER);
	}
	/**
	 * Megjeleníti az ablakot
	 */
	public void visible() {
		jf.pack();
		jf.setDefaultCloseOperation(jf.EXIT_ON_CLOSE);
		tablaPanel.setTablaEnabled(false);
		jf.setVisible(true);
	}
	/**
	 * Ha a játéknak vége, elmentjük az eredményeket!
	 * @throws IOException 
	 * @throws JDOMException 
	 */
	public void Vege() {
		rstJatek();
		Eredmeny e;
		try {
			e = jatek.getEredmeny();
			int n = JOptionPane.showConfirmDialog( jf, "A \"" + e.getNyert() + "\" játékos nyert\nSzeretné menteni az eredményt?", "Eredmény mentése", JOptionPane.YES_NO_OPTION);
			System.out.println(n);
			if(n == 0) {
				new EredmenyekXML().UjEredmeny(e);
			}
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JDOMException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		jatek = null;
		ss = null;
		tablaPanel.setTablaEnabled(false);
	}
	/**
	 * alaphelyzetbe állítja a játékot
	 */
	public void rstJatek() {
		stopClientAction();
		tablaPanel.clrTable();
	}
}
