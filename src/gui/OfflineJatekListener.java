package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import sakk.IJatek;
import sakk.Jatek;

/**
 * Offline játék megkezdésére szolgál
 * @author karsazoltan
 *
 */
public class OfflineJatekListener extends JatekListeners {
	public OfflineJatekListener(sakkGUI s, TablaPanel p) {
		super(s, p);
	}
	/**
	 * az akciót kezelő függvény
	 */
	public void actionPerformed(ActionEvent arg0) {
		String s1 = (String) JOptionPane.showInputDialog(jatekgui.jf, "A fehér játékos neve:", "Játékos nevek", JOptionPane.PLAIN_MESSAGE);
		if(s1 != null) {
			String s2 = (String) JOptionPane.showInputDialog(jatekgui.jf, "A fekete játékos neve:", "Játékos nevek", JOptionPane.PLAIN_MESSAGE);
			if(s2 != null) {
				jatekgui.rstJatek();
				jatekgui.jatek = new Jatek(s1, s2);
				try {
					tablapanel.ujJatek(jatekgui);
					tablapanel.setTablaEnabled(true);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}