package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.JOptionPane;

import sakk.Jatek;

/**
 * A mentés gomb figyelésére hívatott, elkéri a fájl nevét is
 * @author karsazoltan
 *
 */
public class MentListener extends JatekListeners {
	public MentListener(sakkGUI sgui) {
		super(sgui, null);
	}
	/**
	 * Az akció kezelőfüggvénye
	 */
	public void actionPerformed(ActionEvent arg0) {
		if(jatekgui.jatek != null) {
				String s = (String) JOptionPane.showInputDialog(jatekgui.jf, "Adja meg a fájl nevét\nMEGJEGYZÉS: Ha kliensként csatlakozott egy játékhoz, és azt szeretné \nlementeni, a hálózat miatt ez több időt is igénybe vehet,\nvárjon türelemmel!", "Játék mentése", JOptionPane.PLAIN_MESSAGE);
				if(s!=null && s.length() > 0) {
					try {
						//ha client vagy server lenne a getJatek adja meg a referenciát a valós Jatek dologra: 
						Jatek.Ment(s, jatekgui.jatek.getJatek());
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
		} else {
			JOptionPane.showMessageDialog(jatekgui.jf, "Még nem kezdődött el a játék, nem tudom lementeni", "HIBA", JOptionPane.ERROR_MESSAGE);
		}
	}
}
