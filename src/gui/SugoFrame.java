package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.FileReader;

import javax.swing.BoxLayout;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;

/**
 * a súgót megjelenítő osztály
 * @author karsazoltan
 *
 */
public class SugoFrame extends JFrame {
	/**
	 * a sugóhoz tartozó szöveg
	 */
	String szoveg;
	public SugoFrame() {
		super("Súgó");
		if(!xmlOlvas("sugo.html")) {
			JOptionPane.showMessageDialog(this, "Hiba a beolvasás során", "HIBA", JOptionPane.ERROR_MESSAGE);
		}
		JEditorPane jEditorPane = new JEditorPane();
        jEditorPane.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(jEditorPane);
        HTMLEditorKit kit = new HTMLEditorKit();
        jEditorPane.setEditorKit(kit);
        Document doc = kit.createDefaultDocument();
        jEditorPane.setDocument(doc);
        jEditorPane.setText(szoveg);
        getContentPane().add(scrollPane, BorderLayout.CENTER);
		setMaximumSize(new Dimension(400, 1000));
		setMinimumSize(new Dimension(400, 300));
		setPreferredSize(new Dimension(400, 500));
		pack();
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setVisible(true);
	}
	/**
	 * a html fájl olvasása (onnan vesszük a szöveget)
	 * @param fn
	 * @return
	 */
	private boolean xmlOlvas(String fn) {
		try {
			szoveg = "";
			FileReader fr = new FileReader(fn);
			BufferedReader br = new BufferedReader(fr);
			while(br.ready()) {
				szoveg += br.readLine();
			}
			br.close();
		} catch (Exception e) {
			return false;
		} 
		return true;
	}
}
