package gui;

import java.awt.event.ActionListener;

import javax.swing.JPanel;

/**
 * Abstract osztály a Listeners-ök számára
 * @author karsazoltan
 *
 */
public abstract class JatekListeners implements ActionListener {
	protected TablaPanel tablapanel;
	protected sakkGUI jatekgui;
	/**
	 * Alap konstruktor
	 * @param gui a játékhoz tartozó grafikus osztály
	 * @param s a játék táblájához tartozó osztály
	 */
	public JatekListeners(sakkGUI gui, TablaPanel s) {
		tablapanel = s;
		jatekgui = gui;
	}
}
