package gui;

import java.awt.event.ActionEvent;
import java.io.File;
import java.rmi.RemoteException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import sakk.Babu.Szin;
import sakk.Jatek;
import sakk.SakkClient;
import sakk.SakkServer;

/**
 * Feladata a szerverhez tartozó gomb lenyomásának lekezelése
 */
public class ServerListener extends JatekListeners {
	private boolean mentett;
	/**
	 * KOnstruktor
	 * @param sgui a grafikus játékinterfész egy példánya
	 * @param p azon panel, ahol a mezők vannak
	 * @param mentett_jatek ha mentett játékot kívánunk a szerverre visszatölteni
	 */
	public ServerListener(sakkGUI sgui, TablaPanel p, boolean mentett_jatek) {
		super(sgui, p);
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(mentett) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
			if(fileChooser.showOpenDialog(jatekgui.jf) == JFileChooser.APPROVE_OPTION) {
				jatekgui.jatek = Jatek.Betolt(fileChooser.getSelectedFile().getAbsolutePath());
				if(jatekgui.jatek != null) {
					try {
						//ez biztos hogy Jatek lesz
						SakkServer uj = new SakkServer((Jatek) jatekgui.jatek);
						uj.start();
						jatekgui.setServer(uj);
						tablapanel.ujJatek(jatekgui);
						jatekgui.jatek = new SakkClient("localhost", Szin.feher);
						jatekgui.RemoteActionStart();
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} else {
			try {
				String s1 = (String) JOptionPane.showInputDialog(jatekgui.jf, "A fehér játékos neve:", "Játékos nevek", JOptionPane.PLAIN_MESSAGE);
				if(s1 != null) {
					String s2 = (String) JOptionPane.showInputDialog(jatekgui.jf, "A fekete játékos neve:", "Játékos nevek", JOptionPane.PLAIN_MESSAGE);
					if(s2 != null) {
						SakkServer uj = new SakkServer(s1, s2);
						uj.start();
						jatekgui.setServer(uj);
						JOptionPane.showMessageDialog(jatekgui.jf, "A szerver sikeresen elindult! A kliensek csatlakozásához ossza meg IP-címét");
						jatekgui.jatek = new SakkClient("localhost", Szin.feher); // a szerver indító lesz alapból a fehér játékos
						if(jatekgui.jatek != null) {
							try {
								tablapanel.ujJatek(jatekgui);
								jatekgui.RemoteActionStart();
							} catch (RemoteException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
