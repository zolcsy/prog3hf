package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import sakk.SzabalytalanLepes;
import sakk.Babu;
import sakk.Babu.Szin;
/**
 * A játék mezőinek kezelésére szolgáló ActionListener implementáció
 * @author karsazoltan
 *
 */
public class MezoListener extends JatekListeners {
	private MJButton[][] buttons;
	/**
	 * KOnstruktor
	 * @param sgui a játék grafikus interfészéhez tartozó osztály
	 * @param btns a mezők gomb megfelelője
	 */
	public MezoListener(sakkGUI sgui, TablaPanel tp, MJButton[][] btns) {
		super(sgui, tp);
		buttons = btns;
	}
	/**
	 * az akciót kezelő függvény
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			MJButton mjb = (MJButton) e.getSource();
			try {
				if(jatekgui.jatek.pick(mjb.x, mjb.y)) {
					tablapanel.valtozas(jatekgui, false);
					Babu[] levett = jatekgui.jatek.CsereVan(Szin.feher);
					Babu ok;
					if(levett != null) { 
						ok = (Babu) JOptionPane.showInputDialog(jatekgui.jf, "Egyik bábuddal beértél, válassz cserét!", "Paraszt Csere", JOptionPane.PLAIN_MESSAGE, null, levett, null); 
						jatekgui.jatek.CsereKezel(ok);
						tablapanel.valtozas(jatekgui, true);
					} else {
						levett = jatekgui.jatek.CsereVan(Szin.fekete);
						if(levett != null) {
							ok = (Babu) JOptionPane.showInputDialog(jatekgui.jf, "Egyik bábuddal beértél, válassz cserét!", "Paraszt Csere", JOptionPane.PLAIN_MESSAGE, null, levett, null); 
							jatekgui.jatek.CsereKezel(ok);
							tablapanel.valtozas(jatekgui, true);
						}
					}
					if(jatekgui.jatek.getOnline()) {
						//ha már léptünk, letiltjuk
						tablapanel.setTablaEnabled(false);
					}
				}
				else {
					if(jatekgui.jatek.isNyert())
						jatekgui.Vege();
					else 
						tablapanel.setColor(new Color(80, 160, 90), jatekgui.jatek);
				}
			} catch (SzabalytalanLepes el) {
				JOptionPane.showMessageDialog(jatekgui.jf, el.getMessage(), "HIBA", JOptionPane.ERROR_MESSAGE);
			}
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
