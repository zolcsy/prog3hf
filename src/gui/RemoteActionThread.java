package gui;

import java.net.ConnectException;
import java.rmi.RemoteException;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import sakk.Babu.Szin;
import sakk.IJatek;
import sakk.SakkClient;
/**
 * Az osztály arra szolgál, hogy távoli kapcsolat esetén kérdezze le, változott-e a Játék állapota 
 * @author karsazoltan
 *
 */
public class RemoteActionThread extends Thread {
	private SakkClient sakkclient;
	private sakkGUI sgui;
	private TablaPanel tabla;
	private int sleeptime;
	private volatile boolean ok;
	private volatile boolean ellenfel_ok;
	/**
	 * Konstruktor
	 * @param sc egy csatlakozott sakkclient osztály
	 * @param sg a sakk grafikus interfésze
	 */
	public RemoteActionThread(SakkClient sc, sakkGUI sg, TablaPanel tp)  {
		this(sc, sg, tp, 2500);
	}
	/**
	 * 
	 * @param sc egy menyitott (kapcsolat felépült) sakkclient osztály
	 * @param sg a sakk grafikus interfésze
	 * @param sleep milyen időközönként kérdezgesse a szervert a változásról?
	 */
	public RemoteActionThread(SakkClient sc, sakkGUI sg, TablaPanel tp, int sleep) {
		sakkclient = sc;
		sgui = sg;
		ok = true;
		ellenfel_ok = false;
		sleeptime = sleep;
		tabla = tp;
		try {
			ellenfel_ok = sc.getEllenfelOK();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tabla.setTablaEnabled(false);
	}
	public void stopAction() {
		ellenfel_ok = false;
		ok = false;
	}
	/**
	 * A Thread osztály run() metódusának felüldefiniálása
	 * Feladata a távoli szerver kérdezgetése, van-e változás <code>sakkclient.tavoliValtozas()</code> függvényen keresztül, ha van, lekéri ezeket és beállítja a kliens grafikus interfészét
	 */
	@Override
	public void run() {
		JOptionPane.showMessageDialog(sgui.jf, "Várakozás az ellenfél csatlakozására...");
		while(!ellenfel_ok) {
			try {
				sleep(5000);
				ellenfel_ok = sakkclient.getEllenfelOK();
			} catch (InterruptedException | RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		tabla.setTablaEnabled(true);
		JOptionPane.showMessageDialog(sgui.jf, "Az ellenfél csatlakozott, léphet!");
		while(ok) {
			try {
				sleep(sleeptime);
				if(sakkclient.tavoliValtozas()) {
					int[][] v = sakkclient.valtozas();
					tabla.setButtons(v[0][0], v[0][1], sakkclient.getMezo(v[0][0], v[0][1]).getBabu().iconIndex());
					tabla.setButtonsNullIcon(v[1][0], v[1][1]);
					if(sakkclient.EllenfelCserel()) {
						JOptionPane.showMessageDialog(sgui.jf, "Az ellenfél éppen bábút cserél!...");
					} else {
						tabla.setTablaEnabled(true);
					}
				}
			} catch (InterruptedException e) {
				JOptionPane.showMessageDialog(sgui.jf, e.getMessage(), "HIBA", JOptionPane.ERROR_MESSAGE);
				ok = false; //végeztünk...
				//e.printStackTrace();
			} catch (RemoteException e) {
				JOptionPane.showMessageDialog(sgui.jf, e.getMessage(), "HIBA", JOptionPane.ERROR_MESSAGE);
				ok = false;
				//e.printStackTrace();
			}
		}
	}
}
