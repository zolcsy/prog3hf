package gui;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import sakk.Eredmeny;

/**
 * Az eredmények megjelenítéséhez szükséges TableModel
 * @author karsazoltan
 *
 */
public class EredmenyData extends AbstractTableModel {
	/**
	 * eredmények gyűjteménye 
	 */
	List<Eredmeny> eredmenyek;
	
	/**
	 * KOnstruktor, lista átadásával
	 * @param list az eredmények listája
	 */
	public EredmenyData(List<Eredmeny> list) {
		eredmenyek = list;
	}
	/**
	 * az sozlopok száma
	 */
	@Override
	public int getColumnCount() {
		return 5;
	}
	/**
	 * Az oszlopok osztályai
	 */
	@Override
	public Class<?> getColumnClass(int col) {
		switch(col) {
			case 0: 
			case 1: 
			case 2:
				return String.class; 
			case 3:
				return Boolean.class;
			case 4:
				return LocalDate.class;
		}
		return Object.class;
	}
	/**
	 * lista mérete, sorok száma
	 */
	@Override
	public int getRowCount() {
		return eredmenyek.size();
	}
	/**
	 * az oszlop nevei
	 */
	@Override
	public String getColumnName(int col) {
		switch(col) {
			case 0: return "Fehér játékos";
			case 1: return "Fekete Játékos";
			case 2: return "Nyert";
			case 3: return "Online játék";
			case 4: return "Játéknak vége";
		}
		return null;
	}
	/**
	 * Megadja az adott sorhoz és oszlophoz tartozó értéket
	 */
	@Override
	public Object getValueAt(int row, int col) {
		Eredmeny e = eredmenyek.get(row);
		switch(col) {
			case 0: return e.getFeher();
			case 1: return e.getFekete();
			case 2: return e.getNyert();
			case 3: return e.isOnline();
			case 4: return e.getIdo();
		}
		return null;
	}

}
