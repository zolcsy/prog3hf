package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import org.jdom2.JDOMException;

import sakk.Eredmeny;
import sakk.EredmenyekXML;
/**
 * A menüt megvalósító osztály
 * @author karsazoltan
 *
 */
public class Menu extends JMenuBar {
	/**
	 * menügombok
	 */
	private JMenuItem sugo;
	private JMenuItem mi_jatek;
	private JMenuItem mi_ment;
	private JMenuItem mi_tolt;
	private JMenuItem mi_online;
	private JMenuItem mi_server;
	private JMenuItem mi_servertolt;
	private JMenuItem mi_beallitasok;
	private JMenuItem mi_eredmenyek;
	/**
	 * alap konstruktor, beállítja a neveiket
	 */
	public Menu() {
		JMenu jm = new JMenu("Játék");
		JMenu egyeb = new JMenu("Egyéb");
		sugo = new JMenuItem("Súgó");
		mi_jatek = new JMenuItem("Új Játék");
		mi_ment = new JMenuItem("Mentés");
		mi_tolt = new JMenuItem("Betöltés");
		mi_online = new JMenuItem("Csatlakozás online játékhoz");
		mi_server = new JMenuItem("Új sakk szerver");
		mi_servertolt = new JMenuItem("Új sakk szerver mentett játékkal");
		mi_eredmenyek = new JMenuItem("Eredmények");
		this.add(jm);
		jm.add(mi_jatek);
		jm.add(mi_online);
		jm.add(mi_server);
		jm.add(mi_servertolt);
		jm.add(mi_ment);
		jm.add(mi_tolt);
		this.add(egyeb);
		egyeb.add(mi_eredmenyek);
		egyeb.add(sugo);
	}
	/**
	 * a menühöz tartozó kezelőfüggvényeket állítja be
	 * @param sgui a játék grafikus megvalósítása
	 * @param tablaPanel a tábla grafikus megvalósítása
	 */
	public void setListeners(sakkGUI sgui, TablaPanel tablaPanel) {
		/**
		 * Új offline játékot kezdünk..
		 */
		mi_jatek.addActionListener(new OfflineJatekListener(sgui, tablaPanel));
		/**
		 * Játék lementésére szolgáló ActionListener
		 */
		mi_ment.addActionListener(new MentListener(sgui));
		/**
		 * Lementett játék betöltése offline módba
		 */
		mi_tolt.addActionListener(new BetoltListener(sgui, tablaPanel));
		/**
		 * Új szerver létrehozása, mentett játékkal
		 */
		mi_servertolt.addActionListener(new ServerListener(sgui, tablaPanel, true));
		/**
		 * Távoli sakkszerverhez való csatlakozás
		 */
		mi_online.addActionListener(new ConnectListener(sgui, tablaPanel));
		/**
		 * Sakkszerver létrehozása
		 */
		mi_server.addActionListener(new ServerListener(sgui, tablaPanel, false));
		/**
		 * Súgó frame
		 */
		sugo.addActionListener(ae -> new SugoFrame());
		
		mi_eredmenyek.addActionListener(
			ae -> { 
				List<Eredmeny> list;
				try {
					list = new EredmenyekXML().EredmenyOlvas();
					new EredmenyTableFrame(list).setVisible(true);
				} catch (JDOMException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		});
	}
}
