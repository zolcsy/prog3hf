package gui;

import java.awt.event.ActionEvent;
import java.io.File;
import java.rmi.RemoteException;

import javax.swing.JFileChooser;
import javax.swing.JPanel;

import sakk.Jatek;

/**
 * Offline játékbetöltés kezelsére szolgál
 * @author karsazoltan
 *
 */
public class BetoltListener extends JatekListeners {
	/**
	 * Konstruktor
	 * @param sgui a játék grafikus interfész megvalósítása
	 * @param p a mezőkhöz tartozó panel
	 */
	public BetoltListener(sakkGUI sgui, TablaPanel p) {
		super(sgui, p);
	}
	/**
	 * Offline játékhoz tartozó menüpont kezeléséhez tartozó függvény, megnyit egy fájlkiválasztó ablakot
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
		if(fileChooser.showOpenDialog(jatekgui.jf) == JFileChooser.APPROVE_OPTION) {
			jatekgui.rstJatek();
			jatekgui.jatek = Jatek.Betolt(fileChooser.getSelectedFile().getAbsolutePath());
			tablapanel.setTablaEnabled(true);
			if(jatekgui.jatek != null) {
				try {
					tablapanel.ujJatek(jatekgui);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
