package sakk;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import sakk.Babu.Szin;

/**
 * Az osztály a játék tábláját jelenti, ilyen szempontból csak az ehhez szükséges dolgok vannak itt.
 * @author karsazoltan
 *
 */
public class Tabla implements Serializable {
	/**
	 * sakk van?
	 */
	private boolean sakk;
	private boolean sakkmatt;
	public boolean Sakkmatt() {
		return sakkmatt;
	}
	protected boolean sakkEllenoriz(Szin sz) {
		Mezo mk;
		if(sz == Szin.feher)
			mk = feherKiraly;
		else
			mk = feketeKiraly;
		return ((Kiraly) mk.getBabu()).sakkVizsgalat(mk.getX(), mk.getY(), this);
	}
	protected void setKiraly(int x, int y) {
		Mezo m = getMezo(x, y);
		if(m.getBabu().szin == Szin.feher)
			feherKiraly = m;
		else
			feketeKiraly = m;
	}
	public boolean getSakk() { return sakk; }
	private Mezo sakkokozo;
	public Mezo sakkOkozo() { return sakkokozo; }
	/**
	 * ki van sakkban
	 */
	private Szin Sakkszin;
	/**
	 * melyik játékos jön?
	 */
	private Szin jon;
	/**
	 * ha egy paraszt áttér a szeközti sávba, megmondja hol áll 
	 */
	private Mezo csere;
	/**
	 * Ki a soron következő játékos
	 * @return
	 */
	public Szin kiLep() {
		return jon;
	}
	/**
	 * A tábla 8x8as mezői
	 */
	private Mezo[][] tabla;
	/**
	 * Hol áll a fehér király
	 */
	private Mezo feherKiraly;
	/**
	 * Hol áll a fekete király
	 */
	private Mezo feketeKiraly;
	/**
	 * A levett fekete bábúk gyűjtőhelye (parasztokat nem tárolunk :) )
	 */
	private List<Babu> levett_fekete;
	/**
	 * A levett fehér bábúk helye
	 */
	private List<Babu> levett_feher;
	/**
	 * Mit akarunk levenni
	 */
	private Babu levetel;
	/**
	 * a változott mezők tömbje (indexek)
	 */
	private int[][] valtozott;
	/**
	 * Visszadja a levett bábút
	 * @return
	 */
	public Babu leveheto() { return levetel; }
	/**
	 * Konstruktor, inicializája a változókat, feltölti a táblát
	 */
	public Tabla() {
		csere = null;
		jon = Szin.feher;
		sakk = false;
		tabla = new Mezo[8][8];
		for (int i = 0; i < tabla.length; i++) {
			for (int j = 0; j < tabla.length; j++)
				tabla[i][j] = new Mezo(i, j);
		}
		levett_fekete = new ArrayList<Babu>();
		levett_feher = new ArrayList<Babu>();
		valtozott = new int[][] { new int[]{-1, -1}, new int[]{-1,-1}};
		feltolt();
	}
	private Tabla(Tabla t) {
		tabla = new Mezo[8][8];
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				tabla[i][j] = new Mezo(i, j);
				tabla[i][j].setMezo(t.getBabu(i, j));
				tabla[i][j].jo = false;
			}
		}
		feherKiraly = tabla[t.feherKiraly.getX()][t.feherKiraly.getY()];
		feketeKiraly = tabla[t.feketeKiraly.getX()][t.feketeKiraly.getY()];
	}
	/**
	 * feltölti az induló bábukkal a táblát
	 */
	private void feltolt() {
		for (int i = 0; i < tabla.length; i++) {
			Paraszt p = new Paraszt(Szin.feher);
			tabla[1][i].setMezo(p);
		}
		tabla[0][0].setMezo(new Bastya(Szin.feher));
		tabla[0][7].setMezo(new Bastya(Szin.feher));
		tabla[0][1].setMezo(new Lo(Szin.feher));
		tabla[0][6].setMezo(new Lo(Szin.feher));
		tabla[0][2].setMezo(new Futo(Szin.feher));
		tabla[0][5].setMezo(new Futo(Szin.feher));
		tabla[0][4].setMezo(new Kiraly(Szin.feher));
		feherKiraly = tabla[0][4];
		tabla[0][3].setMezo(new Kiralyno(Szin.feher));

		for (int i = 0; i < tabla.length; i++) {
			Paraszt p = new Paraszt(Szin.fekete);
			tabla[6][i].setMezo(p);
		}
		tabla[7][0].setMezo(new Bastya(Szin.fekete));
		tabla[7][7].setMezo(new Bastya(Szin.fekete));
		tabla[7][1].setMezo(new Lo(Szin.fekete));
		tabla[7][6].setMezo(new Lo(Szin.fekete));
		tabla[7][2].setMezo(new Futo(Szin.fekete));
		tabla[7][5].setMezo(new Futo(Szin.fekete));
		tabla[7][3].setMezo(new Kiraly(Szin.fekete));
		feketeKiraly = tabla[7][3];
		tabla[7][4].setMezo(new Kiralyno(Szin.fekete));
	}
	/**
	 * Adott mezőt adja vissza
	 * @param i sor
	 * @param j oszlop
	 * @return
	 */
	public Mezo getMezo(int i, int j) {
		return tabla[i][j];
	}
	/**
	 * Adott koordinátán álló Bábut adja vissza
	 * @param x
	 * @param y
	 * @return
	 */
	public Babu getBabu(int x, int y) {
		return tabla[x][y].getBabu();
	}
	/**
	 * Visszadja mely mezők változtak (koordinátákkal)
	 * @return
	 */
	public int[][] valtozas(){
		return valtozott;
	}
	/**
	 * A bábú léptethető vele
	 * @param m honna, melyik mezőről lépünk el
	 * @param lastpick a korábban használt mező
	 * @return változott-e a helyzet
	 * @throws SzabalytalanLepes
	 */
	public boolean Lepes(Mezo m, Mezo lastpick) throws SzabalytalanLepes {
		// ekkor valami már ki van pickelve, így lehet lépni oda, ahonnan indult:
		levetel = null;
		csere = null;
		if (m.equals(lastpick))
			return false;
		if(jon == Szin.feher)
			jon = Szin.fekete;
		else
			jon = Szin.feher;
		if(!m.ures()) {
			Babu levett = m.getBabu();
			levetel(levett);
		}
		m.setMezo(lastpick.getBabu());
		if(!m.getBabu().forgathato()) {
			if(m.getBabu().szin == Szin.feher) {
				if(m.getX() == 7)
					csere = m; //innen megtudjuk hogy hol is van
			} else if(m.getX() == 0)
				csere = m;
			else
				csere = null;
		}
		lastpick.setMezo(null);
		//a változott mezők indexei, a beállításhoz
		valtozott[0][0] = m.getX();
		valtozott[0][1] = m.getY();
		valtozott[1][0] = lastpick.getX();
		valtozott[1][1] = lastpick.getY();
		rstTable();
		sakk = false;
		if(m.getBabu().getID() == 1) {
			if(m.getBabu().szin == Szin.feher)
				feherKiraly = m;
			else
				feketeKiraly = m;
		}
		else {
			if(m.getBabu().szin == Szin.feher) {
				Kiraly k = (Kiraly) feketeKiraly.getBabu();
				sakkokozo = k.sakkVizsgalatOkozo(feketeKiraly.getX(), feketeKiraly.getY(), this);
				if(sakkokozo != null) {
					sakk = true;
					Sakkszin = Szin.fekete; //melyik játékos királya van sakkban?
				} 
			}
			else {
				Kiraly k = (Kiraly) feherKiraly.getBabu();
				sakkokozo = k.sakkVizsgalatOkozo(feherKiraly.getX(), feherKiraly.getY(), this);
				if(sakkokozo != null) {
					sakk = true;
					Sakkszin = Szin.feher;
				}
			}
		}
		return true;
	}
	/**
	 * leveszi a paraméterül átadott bábút, és elhelyezi a megfelelő rekeszbe
	 * @param levett
	 */
	private void levetel(Babu levett) {
		if(levett.forgathato()) {
			//a paraszt bábukat fölösleges a gyűjtőbe rakni, mivel cserélni is fölösleges
			if(levett.szin == Szin.feher)
				levett_feher.add(levett);
			else
				levett_fekete.add(levett);
			levetel = levett;
		}
	}
	/**
	 * A mezők állapotát visszaállítja
	 */
	public void rstTable() {
		for (int j = 0; j < tabla.length; j++)
			for (int i = 0; i < tabla.length; i++)
				tabla[j][i].jo = false;
	}
	/**
	 * Paraszt bábú cseréje, ha már beért!
	 * @param hol melyik parasztot
	 * @param mire mire cseréljük
	 */
	public Babu cserelParaszt(Babu mire) {
		Mezo hol = csere;
		if(hol.getBabu().szin == Szin.feher || mire.szin == Szin.feher) {
			int i = LevettBabu(mire);
			valtozott[0][0] = hol.getX();
			valtozott[0][1] = hol.getY();
			hol.setMezo(mire);
			return levett_feher.remove(i);
		}	
		if(hol.getBabu().szin == Szin.fekete || mire.szin == Szin.fekete) {
			int i = LevettBabu(mire);
			valtozott[0][0] = hol.getX();
			valtozott[0][1] = hol.getY();
			hol.setMezo(mire);
			return levett_feher.remove(i);
		}	
		return null;
	}
	private int LevettBabu(Babu keresett) {
		if(keresett.szin == Szin.feher) {
			return levett_feher.indexOf(keresett);
		} else {
			return levett_fekete.indexOf(keresett);
		}
	}
	public Mezo CsereVan() {
		return csere;
	}
	public Babu[] Cserek(Szin sz) {
		Set<Babu> halmaz;
		if(sz == Szin.feher)
			halmaz = new HashSet<Babu>(levett_feher);
		else
			halmaz = new HashSet<Babu>(levett_fekete);
		if(halmaz.size() == 0)
			return null;
		Babu[] vissza = new Babu[halmaz.size()];
		int i = 0;
		for(Babu b : halmaz) {
			vissza[i++] = b;
		}
		return vissza;
	}
	public List<Mezo> getLephetokSakkban() {
		List<Mezo> lephetunk_vele = new ArrayList<Mezo>();
		//annak vizsgálata, hogy a király elléphet-e valahova!
		Mezo kiraly; 
		if(Sakkszin == Szin.feher)
			kiraly = feherKiraly;
		else
			kiraly = feketeKiraly;
		
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				if(!tabla[i][j].ures()) {
					if(tabla[i][j].getBabu().szin == kiraly.getBabu().szin) {
						sakkFeloldok();
						if(tabla[i][j].getBabu().HovaLephetekSakkban(tabla[i][j], this) != 0)
							lephetunk_vele.add(tabla[i][j]);
					}
				}
			}
		}
		
		if(lephetunk_vele.size() == 0)
			sakkmatt = true;
		rstTable();
		return lephetunk_vele;
	}
	/**
	 * Beállítja azon mezőket jó-ra, ahova lépve a sakk feloldható!
	 */
	public void sakkFeloldok() {
		rstTable();
		Mezo kiraly; 
		if(Sakkszin == Szin.feher)
			kiraly = feherKiraly;
		else
			kiraly = feketeKiraly;
		//ha a sakkokozó rekurzív tulajdonságú, meg kell határozni, a közte és a király közötti részt!
		if(sakkokozo.getBabu().rekurziv()) {
			sakkokozo.getBabu().HovaLephetek(sakkokozo, this);
			//ezután az ő és a király közti részt választjuk ki!
			int min_x, max_x, min_y, max_y;
			if(sakkokozo.getX() < kiraly.getX()) {
				min_x = sakkokozo.getX();
				max_x = kiraly.getX();
			} else {
				min_x = kiraly.getX();
				max_x = sakkokozo.getX();
			}
			if(sakkokozo.getY() < kiraly.getY()) {
				min_y = sakkokozo.getY();
				max_y = kiraly.getY();
			} else {
				min_y = kiraly.getY();
				max_y = sakkokozo.getY();
			}
			for(int x = 0; x < 8; x++) {
				for(int y = 0; y < 8; y++) {
					if(x > min_x && x < max_x)
						if(y > min_y && y < max_y)
							tabla[x][y].jo = tabla[x][y].jo && true;
						else
							tabla[x][y].jo = false;
					else
						tabla[x][y].jo = false;
				}
			}
		} 
		//ha leütjük a sakkokozót, jó:
		tabla[sakkokozo.getX()][sakkokozo.getY()].jo = true;
	}
	public Tabla masolat() {
		return new Tabla(this);
	}
	protected static int maszkol(Tabla t1, Tabla t2) {
		int db = 0;
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				t1.tabla[i][j].jo = t1.tabla[i][j].jo && t2.tabla[i][j].jo;
				if(t1.tabla[i][j].jo) 
					db++;
			}
		}
		return db;
	}
}
