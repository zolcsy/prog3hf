package sakk;

import java.rmi.RemoteException;
import sakk.Babu.Szin;
import sakk.SzabalytalanLepes;

/**
 * Általános interfész minden a Játékkal kapcsolatos osztályok megfelelő használatára, pl.: Jatek, SakkClient, SakkServer
 * @author karsazoltan
 *
 */
public interface IJatek {
	/**
	 * 
	 * @return Visszadja a korábban levett Bábut, feltéve ha volt, különben null-t ad vissza
	 * @throws RemoteException
	 */
	public Babu leveheto() throws RemoteException;
	/**
	 * Megadja a fehér játékos nevét
	 * @return fehér játékos neve
	 * @throws RemoteException
	 */
	public String getFeher() throws RemoteException;
	/**
	 * Megadja a fekete játékos nevét
	 * @return fekete játékos neve
	 * @throws RemoteException
	 */
	public String getFekete() throws RemoteException;
	/**
	 * Megadja a kiválasztott mezőt
	 * @param i oszlop
	 * @param j sor
	 * @return az [i,j] mező
	 * @throws RemoteException
	 */
	public Mezo getMezo(int i, int j) throws RemoteException;
	/**
	 * Megadja az x,y helyen álló bábút
	 * @param x oszlop
	 * @param y sor
	 * @return adott bábú, különben null
	 * @throws RemoteException
	 */
	public Babu getBabu(int x, int y) throws RemoteException;
	/**
	 * Kiválaszt egy adott Bábut lépésre, vagy lépteti azt 
	 * @param x oszlop
	 * @param y sor
	 * @return történt-e valóban lépés
	 * @throws RemoteException
	 * @throws SzabalytalanLepes
	 */
	public boolean pick(int x, int y) throws RemoteException, SzabalytalanLepes;
	/**
	 * Ha van változás itt megkapható 
	 * @return A változott mezők koordinátái
	 * @throws RemoteException
	 */
	public int[][] valtozas() throws RemoteException;
	/**
	 * Ki lép éppen
	 * @return az éppen aktuális játékos
	 * @throws RemoteException
	 */
	public Szin kiLep() throws RemoteException;
	/**
	 * A szerializálás miatt, visszaad egy Jatek referenciát, lsd.: SakkClient, SakkServer
	 * @return A tartalmazott Játék objektum, legyen az távoli, vagy helyi
	 * @throws RemoteException
	 */
	public Jatek getJatek() throws RemoteException;
	/**
	 * Megadja, hogy a példány Online vagy offline játszmára való, vagy ahhoz csatlakozik
	 * @return
	 * @throws RemoteException
	 */
	public boolean getOnline() throws RemoteException;
	/**
	 * Egy beért paraszt bábú cseréje a paraméterül kapott bábura
	 * @param b mire szeretnénk lecserélni
	 * @return visszaadja a paraméterül kapott bábút, feltéve hogy volt olyan levett bábunk
	 * @throws RemoteException
	 */
	public Babu CsereKezel(Babu b) throws RemoteException;
	/**
	 * Visszadja az ellenség által levett, cserélhető bábuk tömbjét
	 * @param sz kinek a cseréi, levett bábui
	 * @return a levett bábuk tömbje
	 * @throws RemoteException
	 */
	public Babu[] CsereVan(Szin sz) throws RemoteException;
	/**
	 * Egy játék végét jelző, ha valaki sakkmattot adott
	 * @return vége van a játéknak
	 * @throws RemoteException
	 */
	public boolean isNyert() throws RemoteException;
	/**
	 * Visszadja a nyerő játékos színét
	 * @return nyerő
	 * @throws RemoteException
	 */
	public Szin getNyero() throws RemoteException;
	/**
	 * Ha vége a játéknak, elkérhető a játszma eredménye
	 * @return a játék eredménye
	 * @throws RemoteException
	 */
	public Eredmeny getEredmeny() throws RemoteException;
}
