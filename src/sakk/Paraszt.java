package sakk;

public class Paraszt extends Babu {
	public Paraszt(Szin s) {
		super(s);
		lep = new Lepesek(new int[] {1} , new int[] {0});
	}
	public Paraszt(Paraszt a) {
		this(a.szin);
	}
	@Override
	public boolean rekurziv() {
		return false;
	}
	@Override
	public boolean forgathato() {
		return false;
	}
	@Override
	public boolean isKiraly() {
		return false;
	}
	@Override
	public int getID() {
		return 6;
	}

	@Override
	/**
	 * Mivel a paraszt ütésnél keresztbe is léphet, felül lesz definiálva, az ős függvénye
	 * @param s ahol a bábú áll
	 * @param tabla a bábuhoz tartozó tábla
	 */
	public void HovaLephetek(Mezo s, Tabla tabla) {
		int x = s.getX();
		int y = s.getY(); 
		if(szin == Szin.feher) {
			if(x == 1) 
				if(tabla.getMezo(x+2, y).ures()) {
					Tabla masolat = tabla.masolat();
					masolat.getMezo(x+2, y).setMezo(this);
					masolat.getMezo(x, y).setMezo(null);
					tabla.getMezo(x+2, y).jo = !masolat.sakkEllenoriz(szin);
				}
		}
		else {
			if(x == 6) 
				if(tabla.getMezo(x-2, y).ures()) {
					Tabla masolat = tabla.masolat();
					masolat.getMezo(x-2, y).setMezo(this);
					masolat.getMezo(x, y).setMezo(null);
					tabla.getMezo(x-2, y).jo = !masolat.sakkEllenoriz(szin);
				}
		}
		int x_l = szin == Szin.feher ? x + 1 : x - 1;
		if (x_l >= 0 && x_l < 8) {
			if (tabla.getMezo(x_l, y).ures()) {
				Tabla masolat = tabla.masolat();
				masolat.getMezo(x_l, y).setMezo(this);
				masolat.getMezo(x, y).setMezo(null);
				tabla.getMezo(x_l, y).jo = !masolat.sakkEllenoriz(szin);
			}
			if (y - 1 >= 0) {
				if (!tabla.getMezo(x_l, y-1).ures())
					if (tabla.getMezo(x_l, y-1).getBabu().szin != szin) {
						Tabla masolat = tabla.masolat();
						masolat.getMezo(x_l, y-1).setMezo(this);
						masolat.getMezo(x, y).setMezo(null);
						tabla.getMezo(x_l, y-1).jo = !masolat.sakkEllenoriz(s.getBabu().szin);
					}
			}
			if(y + 1 < 8) {
				if (!tabla.getMezo(x_l, y+1).ures())
					if (tabla.getMezo(x_l, y+1).getBabu().szin != szin) {
						Tabla masolat = tabla.masolat();
						masolat.getMezo(x_l, y+1).setMezo(this);
						masolat.getMezo(x, y).setMezo(null);
						tabla.getMezo(x_l, y+1).jo = !masolat.sakkEllenoriz(s.getBabu().szin);
					}
			}
		}
	}
	@Override
	public int HovaLephetekSakkban(Mezo s, Tabla tabla) {
		int x = s.getX();
		int y = s.getY(); 
		Tabla maszk = tabla.masolat(); 
		if(szin == Szin.feher) {
			if(x == 1) 
				if(maszk.getMezo(x+2, y).ures())
					maszk.getMezo(x+2, y).jo = true;
		}
		else {
			if(x == 6) 
				if(maszk.getMezo(x-2, y).ures())
					maszk.getMezo(x-2, y).jo = true;
		}
		int x_l = szin == Szin.feher ? x + 1 : x - 1;
		if (x_l >= 0 && x_l < 8) {
			if (maszk.getMezo(x_l, y).ures())
				maszk.getMezo(x_l, y).jo = true;
			if (y - 1 >= 0) {
				if (!maszk.getMezo(x_l, y-1).ures())
					if (maszk.getMezo(x_l, y-1).getBabu().szin != szin)
						maszk.getMezo(x_l, y-1).jo = true;
			}
			if(y + 1 < 8) {
				if (!maszk.getMezo(x_l, y+1).ures())
					if (maszk.getMezo(x_l, y+1).getBabu().szin != szin)
						maszk.getMezo(x_l, y+1).jo = true;
			}
		}
		return Tabla.maszkol(tabla, maszk);
	}
}
