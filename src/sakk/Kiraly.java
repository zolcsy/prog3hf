package sakk;

public class Kiraly extends Babu {
	public Kiraly(Szin s) {
		super(s);
		lep = new Lepesek(new int[] {0, 1} , new int[] {1, 1});
	}
	public Kiraly(Kiraly a) {
		this(a.szin);
	}
	@Override
	public boolean rekurziv() {
		return false;
	}
	@Override
	public boolean forgathato() {
		return true;
	}
	@Override
	public boolean isKiraly() {
		return true;
	}
	@Override
	public int getID() {
		return 1;
	}
	/**
	 * Megadja hova léphet a király a táblán, Mivel a király sakk helyzetbe nem léphet, felül definiáltam az ős metódusát
	 * @param s ahol a király áll
	 * @param tabla a tábla mezői
	 * 
	 */
	@Override
	public void HovaLephetek(Mezo s, Tabla tabla) {
		
		int x = s.getX();
		int y = s.getY();
		for (int d = 0; d < lep.getD(); d++) {
			for (int i = 0; i < 4; i++) {
				int x_l = x;
				int y_l = y;
				do {
					x_l += lep.get(d).getX();
					y_l += lep.get(d).getY();
					if (x_l >= 0 && x_l < 8 && y_l >= 0 && y_l < 8) {
						Babu cel = tabla.getMezo(x_l, y_l).getBabu();
						if (cel != null) {
							if (s.getBabu().szin == cel.szin)
								break;
							else {
								//meg kell vizsgálni, nincs-e sakkban itt más bábuktól!
								tabla.getMezo(x_l, y_l).jo = true;
								if(sakkVizsgalat(x_l, y_l, tabla))
									tabla.getMezo(x_l, y_l).jo = false;
								else {
								//a fenti függvény úgy veszi mintha egy "szellem király ott álna, azért a következő ellenőrzés is zszükséges:
									Tabla masolat = tabla.masolat();
									masolat.getMezo(x_l, y_l).setMezo(this);
									masolat.setKiraly(x_l, y_l);
									masolat.getMezo(x, y).setMezo(null);
									tabla.getMezo(x_l, y_l).jo = !masolat.sakkEllenoriz(szin);
								}
								break;
							}
						}
						//meg kell vizsgálni, nincs-e sakkban itt más bábuktól!
						tabla.getMezo(x_l, y_l).jo = true;
						if(sakkVizsgalat(x_l, y_l, tabla))
							tabla.getMezo(x_l, y_l).jo = false;
						else {
							Tabla masolat = tabla.masolat();
							masolat.getMezo(x_l, y_l).setMezo(this);
							masolat.setKiraly(x_l, y_l);
							masolat.getMezo(x, y).setMezo(null);
							tabla.getMezo(x_l, y_l).jo = !masolat.sakkEllenoriz(szin);
						}
						break;
					} else
						break;
				} while (rekurziv());
				lep.forgat();
			}
		}
	}
	@Override
	public int HovaLephetekSakkban(Mezo s, Tabla tabla) {
		//a maszkolát figyelmenkívül hagyjuk, tetszőlegesen elléphetünk, feltéve hogy ott sincs sakk! 
		tabla.rstTable();
		int db = 0;
		int x = s.getX();
		int y = s.getY();
		for (int d = 0; d < lep.getD(); d++) {
			for (int i = 0; i < 4; i++) {
				int x_l = x;
				int y_l = y;
				do {
					x_l += lep.get(d).getX();
					y_l += lep.get(d).getY();
					if (x_l >= 0 && x_l < 8 && y_l >= 0 && y_l < 8) {
						Babu cel = tabla.getMezo(x_l, y_l).getBabu();
						if (cel != null) {
							if (s.getBabu().szin == cel.szin)
								break;
							else {
								//meg kell vizsgálni, nincs-e sakkban itt más bábuktól!
								tabla.getMezo(x_l, y_l).jo = true;
								if(sakkVizsgalat(x_l, y_l, tabla))
									tabla.getMezo(x_l, y_l).jo = false;
								else {
									//a fenti függvény úgy veszi mintha egy "szellem király ott álna, azért a következő ellenőrzés is zszükséges:
										Tabla masolat = tabla.masolat();
										masolat.getMezo(x_l, y_l).setMezo(this);
										masolat.setKiraly(x_l, y_l);
										masolat.getMezo(x, y).setMezo(null);
										tabla.getMezo(x_l, y_l).jo = !masolat.sakkEllenoriz(szin);
								}
								if(tabla.getMezo(x_l, y_l).jo) db++;
								break;
							}
						}
						//meg kell vizsgálni, nincs-e sakkban itt más bábuktól!
						tabla.getMezo(x_l, y_l).jo = true;
						if(sakkVizsgalat(x_l, y_l, tabla))
							tabla.getMezo(x_l, y_l).jo = false;
						else {
							//a fenti függvény úgy veszi mintha egy "szellem" király ott álna, azért a következő ellenőrzés is zszükséges:
								Tabla masolat = tabla.masolat();
								masolat.getMezo(x_l, y_l).setMezo(this);
								masolat.setKiraly(x_l, y_l);
								masolat.getMezo(x, y).setMezo(null);
								tabla.getMezo(x_l, y_l).jo = !masolat.sakkEllenoriz(szin);
						}
						if(tabla.getMezo(x_l, y_l).jo) db++;
						break;
					} else
						break;
				} while (rekurziv());
				lep.forgat();
			}
		}
		return db;
	}
	/**
	 * Annak vizsgálata hogy a bábú sakkban áll-e
	 * @param x a király első koordinátája
	 * @param y a 2. koordináta
	 * @param tabla sakktábla
	 * @return
	 */
	public boolean sakkVizsgalat(int x, int y, Tabla tabla) {
		//futó típus vizsgálata:
		Babu b = new Futo(szin);
		if(Ellenoriz(b, x, y, tabla))
			return true; //ha futó	
		b = new Bastya(szin);
		if(Ellenoriz(b, x, y, tabla))
			return true; //ha bástyajava
		b = new Kiralyno(szin);
		if(Ellenoriz(b, x, y, tabla))
			return true; //királynő
		b = new Lo(szin);
		if(Ellenoriz(b, x, y, tabla))
			return true; //ló
		b = new Kiraly(szin);
		if(Ellenoriz(b, x, y, tabla))
			return true; //másik király
		b = new Paraszt(szin);
		if(Ellenoriz(b, x, y, tabla))
			return true;
		return false;
	}
	/**
	 * Visszadja ki okozza a sakkot
	 * @param x ahol a király áll
	 * @param y ahol a király áll
	 * @param tabla a királyhoz tartozó tábla
	 * @return a sakkot okozó bábú melyik mezőn áll
	 */
	public Mezo sakkVizsgalatOkozo(int x, int y, Tabla tabla) {
		Babu b = new Futo(szin);
		Mezo m = Ellenoriz_SakkOkozo(b, x, y, tabla);
		if(m != null)
			return m;
		b = new Bastya(szin);
		m = Ellenoriz_SakkOkozo(b, x, y, tabla);
		if(m != null)
			return m;
		b = new Kiralyno(szin);
		m = Ellenoriz_SakkOkozo(b, x, y, tabla);
		if(m != null)
			return m;
		b = new Lo(szin);
		m = Ellenoriz_SakkOkozo(b, x, y, tabla);
		if(m != null)
			return m;
		b = new Kiraly(szin);
		m = Ellenoriz_SakkOkozo(b, x, y, tabla);
		if(m != null)
			return m;
		b = new Paraszt(szin);
		m = Ellenoriz_SakkOkozo(b, x, y, tabla);
		if(m != null)
			return m;
		return null;
	}
	/**
	 * Egy adott bábútípussal vizsgálja a sakk helyzetet
	 * @param b melyik bábuval vizsgáljuk
	 * @param x b király első koordinátája
	 * @param y b király 2. koordinátája
	 * @param tabla a sakktábla
	 * @return
	 */
	private boolean Ellenoriz(Babu b, int x, int y, Tabla tabla) {

		if(b.forgathato()) {
			Lepesek lep = b.lep;
			for (int d = 0; d < lep.getD(); d++) {
				for (int i = 0; i < 4; i++) {
					int x_l = x;
					int y_l = y;
					do {
						x_l += lep.get(d).getX();
						y_l += lep.get(d).getY();
						if (x_l >= 0 && x_l < 8 && y_l >= 0 && y_l < 8) {
							Babu cel = tabla.getMezo(x_l, y_l).getBabu();
							//csak az érdekel minket, hogy van-e ott valami
							if (cel != null) {	
								//ha megegyező szín, nem baj, kivéve a saját királyunk esetén
								if (b.szin == cel.szin) {
									break;
								}
								else {
									//ha ellenfél, megnézzük, azonos-e a lepes tulajdonsága:
									if(b.forgathato() == cel.forgathato() && b.rekurziv() == cel.rekurziv()) {
										if(cel.getID() == b.getID()) {
											
											return true;
										}
									}
									break;
								}
							}
						} else
							break;
					} while (b.rekurziv());
					lep.forgat();
				}
			}
		} else { //paraszt
			if(szin == Szin.feher) {
				if(x+1 < 8) {
					if(y -1 >= 0) {
						if(!tabla.getMezo(x+1, y-1).ures()) {
							if(!tabla.getMezo(x+1, y-1).getBabu().forgathato() && tabla.getMezo(x+1, y-1).getBabu().szin != szin)
								return true;
						}
					}
					if(y+1 < 8) {
						if(!tabla.getMezo(x+1, y+1).ures()) {
							if(!tabla.getMezo(x+1, y+1).getBabu().forgathato() && tabla.getMezo(x+1, y+1).getBabu().szin != szin)
								return true;
						}
					}
				}
				
			} else {
				if(x-1 >= 0) {
					if(y+1 < 8) {
						if(!tabla.getMezo(x-1, y+1).ures()) {
							if(!tabla.getMezo(x-1, y+1).getBabu().forgathato() && tabla.getMezo(x-1, y+1).getBabu().szin != szin)
								return true;
						}
					}
					if(y - 1 >= 0) {
						if(!tabla.getMezo(x-1, y-1).ures()) {
							if(!tabla.getMezo(x-1, y-1).getBabu().forgathato() && tabla.getMezo(x-1, y-1).getBabu().szin != szin)
								return true;
						}
					}
				}
			}
		}
		return false;
	}
	/**
	 * Hasonló az ellenőrizhez, megadja, hogy hány adott Bábú típustól van sakkban
	 * @param b milyen bábutól
	 * @param x ahol a király áll
	 * @param y ahol a király áll
	 * @param tabla
	 * @return a a sakkot okozó hol áll
	 */
	private Mezo Ellenoriz_SakkOkozo(Babu b, int x, int y, Tabla tabla) {
		if(b.forgathato()) {
			Lepesek lep = b.lep;
			for (int d = 0; d < lep.getD(); d++) {
				for (int i = 0; i < 4; i++) {
					int x_l = x;
					int y_l = y;
					do {
						x_l += lep.get(d).getX();
						y_l += lep.get(d).getY();
						if (x_l >= 0 && x_l < 8 && y_l >= 0 && y_l < 8) {
							Babu cel = tabla.getMezo(x_l, y_l).getBabu();
							//csak az érdekel minket, hogy van-e ott valami
							if (cel != null) {	
								//ha megegyező szín, nem baj
								if (b.szin == cel.szin) {
									break;
								}
								else {
									//ha ellenfél, megnézzük, azonos-e a lepes tulajdonsága:
									if(b.forgathato() == cel.forgathato() && b.rekurziv() == cel.rekurziv()) {
										if(cel.getID() == b.getID()) {
											return tabla.getMezo(x_l, y_l);
									
										}
									}
									break;
								}
							}
						} else
							break;
					} while (b.rekurziv());
					lep.forgat();
				}
			}
		} else { //paraszt
			if(szin == Szin.feher) {
				if(x+1 < 8) {
					if(y -1 >= 0) {
						if(!tabla.getMezo(x+1, y-1).ures()) {
							if(!tabla.getMezo(x+1, y-1).getBabu().forgathato() && tabla.getMezo(x+1, y-1).getBabu().szin != szin) {
								return tabla.getMezo(x + 1, y - 1);
							}
						}
					}
					if(y+1 < 8) {
						if(!tabla.getMezo(x+1, y+1).ures()) {
							if(!tabla.getMezo(x+1, y+1).getBabu().forgathato() && tabla.getMezo(x+1, y+1).getBabu().szin != szin) {
								return tabla.getMezo(x + 1, y + 1);
							}
						}
					}
				}
				
			} else {
				if(x-1 >= 0) {
					if(y+1 < 8) {
						if(!tabla.getMezo(x-1, y+1).ures()) {
							if(!tabla.getMezo(x-1, y+1).getBabu().forgathato() && tabla.getMezo(x-1, y+1).getBabu().szin != szin) {
								return tabla.getMezo(x - 1, y + 1);
							}
						}
					}
					if(y - 1 >= 0) {
						if(!tabla.getMezo(x-1, y-1).ures()) {
							if(!tabla.getMezo(x-1, y-1).getBabu().forgathato() && tabla.getMezo(x-1, y-1).getBabu().szin != szin) {
								return tabla.getMezo(x - 1, y - 1);
							}
						}
					}
				}
			}
		}
		return null;
	}
	
}
