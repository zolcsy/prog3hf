package sakk;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

import sakk.Babu.Szin;
import sakk.SzabalytalanLepes;
/**
 * Egy sakk szerver létrehozásáért felel, itt tárolódik a valódi Jatek objektum
 * @author karsazoltan
 *
 */
public class SakkServer extends UnicastRemoteObject implements IsakkRMI {
	/**
	 * A valódi játék, ahol zajlik a játszma
	 */
	Jatek j;
	/**
	 * Változott-e a fehér játékos állapota
	 */
	private boolean valtozasFeher;
	/**
	 * Változott a fekte játékos állapota
	 */
	private boolean valtozasFekete;
	/**
	 * A fehér játékos csatlakozott
	 */
	private boolean feher_ok;
	/**
	 * A fekete játékos csatlakozott
	 */
	private boolean fekete_ok;
	/**
	 * Több szerver esetén azonosító megadás lehetséges
	 */
	private int id;
	/**
	 * Logolás legyen-e?
	 */
	private boolean log;
	@Override
	public boolean tavoliValtozas(boolean ki) {
		return ki ? valtozasFeher : valtozasFekete;
	}
	/**
	 * KOnstruktor, a játékosok nevét várja
	 * @param j1 fehér játékos neve
	 * @param j2 fekete játékos neve
	 * @throws RemoteException
	 */
	public SakkServer(String j1, String j2) throws RemoteException {
		j = new Jatek(j1, j2);
		id = 0;
		log = false;
		try { 
		    LocateRegistry.createRegistry(1099); 
		} catch (RemoteException e) {
		    System.out.println("java RMI regiszter létezik.");
		}
	}
	public SakkServer(String j1, String j2, int azon) throws RemoteException {
		j = new Jatek(j1, j2);  
		id = azon;
		log = false;
		try { 
		    LocateRegistry.createRegistry(1099); 
		} catch (RemoteException e) {
		    System.out.println("java RMI regiszter létezik.");
		}
	}
	/**
	 * Mentett játék szerveren történő folytatását teszi lehetővé
	 * @param regi A már betöltött játék objektum
	 * @throws RemoteException
	 */
	public SakkServer(Jatek regi) throws RemoteException {
		j = regi;
		id = 0;
		log = false;
		try { 
		
		    LocateRegistry.createRegistry(1099); 
		} catch (RemoteException e) {
			System.out.println("java RMI regiszter létezik.");
		}
	}
	/**
	 * Elindítja a szervert, "kihelyezi" az objektumot
	 * 
	 */
	public void start() {
		try {
		    //RmiServer példányosítása
		    Naming.rebind("rmi://localhost/SakkServer" + id, this);
		} catch (Exception e) {
		    System.err.println("RMI server hiba:" + e);
		    e.printStackTrace();
		}
		valtozasFeher = false;
		valtozasFekete = false;
		fekete_ok = false;
		feher_ok = false;
	}
	@Override
	public String getFeher() throws RemoteException {
		return j.getFeher();
	}
	@Override
	public boolean pick(int x, int y) throws SzabalytalanLepes, RemoteException {
		boolean temp = j.pick(x, y);
		Szin lepo = j.getBabu(x, y).getSzin();
		if(lepo == Szin.feher) {
			valtozasFeher = temp;
			valtozasFekete = false;
		} else {
			valtozasFeher = false;
			valtozasFekete = temp;
		}
		return temp;
	}
	@Override
	public Babu getBabu(int x, int y) throws RemoteException {
		return j.getBabu(x, y);
	}
	@Override
	public int[][] valtozas() throws RemoteException {
		return j.valtozas();
	}
	@Override
	public Babu leveheto() throws RemoteException {
		return j.leveheto();
	}
	@Override
	public String getFekete() throws RemoteException {
		return j.getFekete();
	}
	@Override
	public Mezo getMezo(int x, int y) throws RemoteException {
		return j.getMezo(x,y);
	}
	@Override
	public Szin kiLep() throws RemoteException {
		return j.kiLep();
	}
	@Override
	public Jatek getJatek() throws RemoteException {
		return j;
	}
	@Override
	public boolean getOnline() throws RemoteException {
		return true;
	}
	@Override
	public boolean getJatekosOK(Szin sz) throws RemoteException {
		if(sz == Szin.feher)
			return feher_ok;
		return fekete_ok;
	}
	@Override
	public void setJatekos(Szin sz) throws RemoteException {
		if(sz == Szin.feher)
			feher_ok = true;
		else
			fekete_ok = true;
			
	}
	@Override
	public Babu CsereKezel(Babu b) throws RemoteException {
		return j.CsereKezel(b);
	}
	@Override
	public Babu[] CsereVan(Szin sz) throws RemoteException {
		return j.CsereVan(sz);
	}
	@Override
	public boolean isNyert() throws RemoteException {
		return j.isNyert();
	}
	@Override
	public Szin getNyero() throws RemoteException {
		return j.getNyero();
	}
	@Override
	public Eredmeny getEredmeny() throws RemoteException {
		if(j.isNyert()) {
			return new Eredmeny(getFeher(), getFekete(), getNyero() == Szin.feher ? getFeher() : getFekete(), getOnline());
		}
		return new Eredmeny(getFeher(), getFekete(), "?", getOnline());
	}
}
