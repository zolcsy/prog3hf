package sakk;

import java.io.Serializable;

/**
 * Abstract osztály a bábuk számára
 * @author karsazoltan
 *
 */
abstract public class Babu implements Serializable {
	/**
	 * A bábú azonosításához (melyik csoportba tartozik) szükséges enum
	 * @author karsazoltan
	 *
	 */
	public enum Szin {feher, fekete};
	/**
	 * A bábú milyen irányba képes lépni
	 */
	public Lepesek lep = null;
	/**
	 * A bábú csoport azonosítója (fehér/fekete) 
	 */
	protected Szin szin; 
	/**
	 * Megadja a bábú színét
	 */
	public Szin getSzin() {
		return szin;
	}
	/**
	 * Konstruktor szín beállításával
	 * @param s
	 */
	public Babu(Szin s) {
		szin = s;
	}
	/**
	 * Megadja a bábú ikonját azonosító számot, a szín függvényében
	 * @param s a bábú milyen színű
	 * @return a bábú ikon sorszáma
	 */
	public int iconIndex() {
		return szin == Szin.feher ? getID() - 1 : getID() + 5;
	}
	/**
	 * A bábú lépése rekurzív típusú
	 * @return true - igen, az, különben false
	 */
	public abstract boolean rekurziv(); 
	/**
	 * A bábú tetszőleges irányba léphet
	 * @return true - igen, false nem léphet
	 */
	public abstract boolean forgathato();
	/**
	 * A bábu király?
	 * @return igen/nem
	 */
	public abstract boolean isKiraly();
	/**
	 * A paraméterül átvett tabla mezőn beállítja, a bábú hova léphet, ez a Paraszt és Király osztályoknál más lesz, így ott felül lesz definiálva
	 * @param s ahol a bábú áll
	 * @param tabla a sakktábla mezői
	 */
	public void HovaLephetek(Mezo s, Tabla tabla) {
		int x = s.getX();
		int y = s.getY();
		for (int d = 0; d < lep.getD(); d++) {
			for (int i = 0; i < 4; i++) {
				int x_l = x;
				int y_l = y;
				do {
					x_l += lep.get(d).getX();
					y_l += lep.get(d).getY();
					if (x_l >= 0 && x_l < 8 && y_l >= 0 && y_l < 8) {
						Babu cel = tabla.getMezo(x_l, y_l).getBabu();
						if (cel != null) {
							if (s.getBabu().szin == cel.szin)
								break;
							else {
								tabla.getMezo(x_l, y_l).jo = true;
								//annak vizsgálata, hogy nem-e lesz sakk ha ellép: (pl takarja a királyt valamitől)
								Tabla masolat = tabla.masolat();
								masolat.getMezo(x_l, y_l).setMezo(this);
								masolat.getMezo(x, y).setMezo(null);
								tabla.getMezo(x_l, y_l).jo = !masolat.sakkEllenoriz(szin);
								break;
							}
						}
						//annak vizsgálata, hogy nem-e lesz sakk ha ellép: (pl takarja a királyt valamitől)
						tabla.getMezo(x_l, y_l).jo = true;
						Tabla masolat = tabla.masolat();
						masolat.getMezo(x_l, y_l).setMezo(this);
						masolat.getMezo(x, y).setMezo(null);
						tabla.getMezo(x_l, y_l).jo = !masolat.sakkEllenoriz(szin);
					} else
						break;
				} while (rekurziv());
				lep.forgat();
			}
		}
	}
	/**
	 * Megmondja, hogy az adott bábú hova léphet ha sakk lép fel!
	 * @param s az a mező, ahol a bábú áll
	 * @param tabla a játék táblája, beállítva azon mezőkkel, ahova lehet lépni a sakk kiütése érdekében!
	 * @return hány helyre léphetünk!
	 */
	public int HovaLephetekSakkban(Mezo s, Tabla tabla) {
		Tabla maszk = tabla.masolat();
		int x = s.getX();
		int y = s.getY();
		for (int d = 0; d < lep.getD(); d++) {
			for (int i = 0; i < 4; i++) {
				int x_l = x;
				int y_l = y;
				do {
					x_l += lep.get(d).getX();
					y_l += lep.get(d).getY();
					if (x_l >= 0 && x_l < 8 && y_l >= 0 && y_l < 8) {
						Babu cel = tabla.getMezo(x_l, y_l).getBabu();
						if (cel != null) {
							if (s.getBabu().szin == cel.szin)
								break;
							else {
								maszk.getMezo(x_l, y_l).jo = true;
								break;
							}
						}
						maszk.getMezo(x_l, y_l).jo = true;
					} else
						break;
				} while (rekurziv());
				lep.forgat();
			}
		}
		return Tabla.maszkol(tabla, maszk);
	}
	/**
	 * Megadja a bábú azonosítóját!
	 * @return id
	 */
	public abstract int getID();
	@Override
	public boolean equals(Object o) {
		Babu b = (Babu) o;
		return b.getID() == this.getID();
	}
}
