package sakk;

import java.rmi.Remote;
import java.rmi.RemoteException;

import sakk.Babu.Szin;
import sakk.SzabalytalanLepes;

/**
 * A hálózati résznek egy interfész, extra dolgokkal
 * @author karsazoltan
 *
 */
public interface IsakkRMI extends Remote, IJatek {
	/**
	 * Ha van távoli változás az adott játékos részéről itt lekérhető az állapota
	 * @param ki true - fehér játékos, false-fekete (hogy ne kelljen az egész )
	 * @return van változás
	 * @throws RemoteException
	 */
	public boolean tavoliValtozas(boolean ki) throws RemoteException;
	/**
	 * Annak vizsgálata, hogy egy játékos sikeresen csatlakozott 
	 * @param sz kérdéses játékos színe
	 * @return igen/nem
	 * @throws RemoteException
	 */
	public boolean getJatekosOK(Szin sz) throws RemoteException;
	/**
	 * A játékos felcsatlakozott, így beállítjuk
	 * @param sz a játékos színe
	 * @throws RemoteException
	 */
	public void setJatekos(Szin sz) throws RemoteException;
}
