package sakk;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import sakk.Babu.Szin;
import sakk.SzabalytalanLepes;

public class SakkClient implements IJatek {
	private IsakkRMI j;
	private String jatekos;
	private Szin jatekos_ff;
	private int id;
	public SakkClient(Szin szin) {
		this("localhost", szin);
	}
	public SakkClient(String ip, Szin sz, int id_) {
		id = id_;
		try {
			j = (IsakkRMI) Naming.lookup("rmi://" + ip + "/SakkServer" + id);
			jatekos = sz == Szin.feher ? j.getFeher() : j.getFekete();
			jatekos_ff = sz;
			j.setJatekos(sz);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public SakkClient(String ip, Szin sz) {
		this(ip, sz, 0);
	}
	@Override
	public Babu leveheto() throws RemoteException {
		return j.leveheto();
	}
	@Override
	public String getFeher() throws RemoteException {
		return j.getFeher();
	}
	@Override
	public String getFekete() throws RemoteException {
		return j.getFekete();
	}
	@Override
	public Mezo getMezo(int x, int y) throws RemoteException {
		return j.getMezo(x, y);
	}
	@Override
	public Babu getBabu(int x, int y) throws RemoteException {
		return j.getBabu(x, y);
	}
	@Override
	public boolean pick(int x, int y) throws RemoteException, SzabalytalanLepes {
		if(jatekos_ff != j.kiLep())
			throw new SzabalytalanLepes("Azzal nem léphetsz!");
		return j.pick(x, y);
	}
	@Override
	public int[][] valtozas() throws RemoteException {
		return j.valtozas();
	}
	public String getJatekos() {
		return jatekos;
	}
	@Override
	public Szin kiLep() throws RemoteException {
		return j.kiLep();
	}
	public boolean tavoliValtozas() throws RemoteException {
		//nem mi voltunk a változtató fél:
		boolean ret = j.tavoliValtozas(jatekos_ff != Szin.feher);
		return ret;
	}
	@Override
	public Jatek getJatek() throws RemoteException {
		return j.getJatek();
	}
	@Override
	public boolean getOnline() throws RemoteException {
		return true;
	}
	/**
	 * A játékos távoli ellenfele csatlakozott, és készen áll-e a játékra
	 * @return igen/nem
	 * @throws RemoteException
	 */
	public boolean getEllenfelOK() throws RemoteException {
		return j.getJatekosOK(jatekos_ff == Szin.feher ? Szin.fekete : Szin.feher);
	}
	public boolean EllenfelCserel() throws RemoteException {
		return j.CsereVan(jatekos_ff == Szin.feher ? Szin.fekete : Szin.feher) != null;
	}
	@Override
	public Babu CsereKezel(Babu b) throws RemoteException {
		return j.CsereKezel(b);
	}
	@Override
	public Babu[] CsereVan(Szin sz) throws RemoteException {
		return j.CsereVan(sz);
	}
	@Override
	public boolean isNyert() throws RemoteException {
		return j.isNyert();
	}
	@Override
	public Szin getNyero() throws RemoteException {
		return j.getNyero();
	}
	@Override
	public Eredmeny getEredmeny() throws RemoteException {
		return j.getEredmeny();
	}
}
