package sakk;

import java.io.Serializable;

/**
 * A bábú milyen lépéseket tehet
 * @author karsazoltan
 *
 */
public class Lepesek implements Serializable {
	/**
	 * a lehetséges lépéshez tartozó vektor
	 */
	Irany iranyok[];
	public Lepesek(int[] x_k, int[] y_k) {
		iranyok = new Irany[x_k.length];
		for(int i =0; i < x_k.length; i++)
			iranyok[i] = new Irany(x_k[i], y_k[i]);
	}
	/**
	 * adott dimenziójú vektor lekérése
	 * @param k dimenzió
	 * @return adott irányvektor
	 */
	public Irany get(int k) {
		return iranyok[k];
	}
	/**
	 * az adott irányvektort elforgatja 
	 * @param k
	 */
	public void forgat(int k) {
		iranyok[k].forgat();
	}
	/**
	 * az összes irányvektort elforgatja
	 */
	public void forgat() {
		for(int i = 0; i < iranyok.length; i++)
			forgat(i);
	}
	/**
	 * megadja a dimenziók számát
	 * @return dimenziók száma
	 */
	public int getD() { return iranyok.length; }
	/**
	 * belső osztály az irányokhoz
	 * @author karsazoltan
	 *
	 */
	public class Irany implements Serializable {
		/**
		 * vektor első koordinátája
		 */
		private int x;
		/**
		 * vektor 2. koordinátája
		 */
		private int y;
		/**
		 * irány beállításához konstruktor
		 * @param x első koordináta
		 * @param y második koordináta
		 */
		public Irany(int x, int y) {
			this.x = x;
			this.y = y;
		}
		/**
		 * 90fokkal elforgatja a vektort
		 */
		public void forgat() {
			int tmp = x;
			x = -y;
			y = tmp;
		}
		/**
		 * Visszaadja az első koordinátát
		 * @return első koor
		 */
		public int getX() {return x;}
		/**
		 * Visszaadja a 2. koordinátát
		 * @return 2. koor
		 */
		public int getY() {return y;}
	}
}
