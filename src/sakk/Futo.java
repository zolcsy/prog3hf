package sakk;

public class Futo extends Babu {
	public Futo(Szin s) {
		super(s);
		lep = new Lepesek(new int[] {1} , new int[] {1});
	}
	public Futo(Futo a) {
		this(a.szin);
	}
	@Override
	public boolean rekurziv() {
		return true;
	}
	@Override
	public boolean forgathato() {
		return true;
	}
	@Override
	public boolean isKiraly() {
		return false;
	}
	@Override
	public int getID() {
		return 3;
	}
}
