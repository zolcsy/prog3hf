package sakk;

/**
 * A játékban szabálytalan lépések jelzésére!
 * @author karsazoltan
 *
 */
public class SzabalytalanLepes extends Exception {
	private String err_msg;
	public SzabalytalanLepes(String msg) {
		err_msg = msg;
	}
	@Override
	public String getMessage() {
		return err_msg;
	}
}