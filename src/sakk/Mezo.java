package sakk;

import java.io.Serializable;
/**
 * A tábla mezőinek reprezentációja a játéktáblán, koordinátákkal
 */
public class Mezo implements Serializable {
	/**
	 * első koordináta
	 */
	private int x;
	/**
	 * második koordináta
	 */
	private int y;
	/**
	 * referencia az ott álló bábúra
	 */
	private Babu b;
	/**
	 * a mező használható
	 */
	public boolean jo; 
	/**
	 * Inicializálás
	 * @param x első
	 * @param y második koordináta
	 */
	public Mezo(int x, int y) {
		this.x = x;
		this.y = y;
		b = null;
	}
	/**
	 * a mezőre állítja a bábút
	 * @param b
	 */
	public void setMezo(Babu b) {
		this.b = b;
	}
	/**
	 * visszadja az itt álló bábút
	 * @return
	 */
	public Babu getBabu() {
		return b;
	}
	/**
	 * áll-e valami a mezőn
	 * @return
	 */
	public boolean ures() {
		return (b == null);
	}
	/**
	 * a mező első koordinátája
	 * @return első koordináta
	 */
	public int getX() {return x;}
	/**
	 * a mező második koordinátája
	 * @return második koordináta
	 */
	public int getY() {return y;}
	@Override
	public boolean equals(Object o) {
		Mezo m = (Mezo) o;
		return x == m.x && y == m.y;
	}
}
