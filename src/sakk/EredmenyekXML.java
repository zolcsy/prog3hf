package sakk;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 * Az eredmények XML letárolásáért felelős osztály
 * jdom2-es XML kezelés
 * @author karsazoltan
 *
 */
public class EredmenyekXML {
	/**
	 * a fájl neve
	 */
	private String fn = "games.xml";
	/**
	 * a fájl nevének átállítása
	 * @param fajlnev
	 */
	public void setMentes(String fajlnev) {
		fn = fajlnev;
	}
	/**
	 * Beolvassa az xml fájlt tartalmát
	 * @return az eredmények gyűjteménye
	 * @throws JDOMException
	 * @throws IOException
	 */
	public List<Eredmeny> EredmenyOlvas() throws JDOMException, IOException {
		File inputFile = new File(fn);
		SAXBuilder saxBuilder = new SAXBuilder();
		Document document = saxBuilder.build(inputFile);
		List<Eredmeny> lista = new ArrayList<Eredmeny>();
		List<Element> xml = document.getRootElement().getChildren();
		for(int i = 0; i < xml.size(); i++) {
			Element game = xml.get(i);
			String feher = game.getChildText("feher");
			String fekete = game.getChildText("fekete");
			String nyert = game.getChildText("nyert");
			boolean online = game.getChildText("online").compareTo("true") == 0;
			LocalDate date = LocalDate.parse(game.getChildText("ido"), DateTimeFormatter.ofPattern("yyyy.MM.dd."));
			lista.add(new Eredmeny(feher, fekete, nyert, online, date));
		}
		return lista;
	}
	/**
	 * Új eredményt ment el az xml fájlba
	 * @param e eredmény
	 * @throws JDOMException
	 * @throws IOException
	 */
	public void UjEredmeny(Eredmeny e) throws JDOMException, IOException {
		File inputFile = new File(fn);
		SAXBuilder saxBuilder = new SAXBuilder();
		Document document = saxBuilder.build(inputFile);
		Element root = document.getRootElement();
		root.addContent(EredmenyElement(e));
		document.setContent(root);
		FileWriter writer = new FileWriter(fn);
        XMLOutputter outputter = new XMLOutputter();
        outputter.setFormat(Format.getPrettyFormat());
        outputter.output(document, writer);
        writer.close();
	}
	/**
	 * létrehoz egy xml Element-et egy eredményből
	 * @param e adott eredmény
	 * @return
	 */
	private static Element EredmenyElement(Eredmeny e) {
		Element el = new Element("game");
		el.addContent(new Element("feher").setText(e.getFeher()));
		el.addContent(new Element("fekete").setText(e.getFekete()));
		el.addContent(new Element("nyert").setText(e.getNyert()));
		el.addContent(new Element("online").setText(e.isOnline() ? "true" : "false"));
		el.addContent(new Element("ido").setText(e.getIdo().format(DateTimeFormatter.ofPattern("yyyy.MM.dd."))));
		return el;
	}
}
