package sakk;

public class Kiralyno extends Babu {
	public Kiralyno(Szin s) {
		super(s);
		lep = new Lepesek(new int[] {0, 1} , new int[] {1, 1});
	}
	public Kiralyno(Kiralyno a) {
		this(a.szin);
	}
	public boolean rekurziv() {
		return true;
	}
	@Override
	public boolean forgathato() {
		return true;
	}
	@Override
	public boolean isKiraly() {
		return false;
	}
	@Override
	public int getID() {
		return 2;
	}
}