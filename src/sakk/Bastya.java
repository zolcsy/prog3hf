package sakk;

public class Bastya extends Babu {
	public Bastya(Szin s) {
		super(s);
		lep = new Lepesek(new int[] {0} , new int[] {1});
	}
	public Bastya(Bastya a) {
		this(a.szin);
	}
	@Override
	public boolean rekurziv() {
		return true;
	}
	@Override
	public boolean forgathato() {
		return true;
	}
	@Override
	public boolean isKiraly() {
		return false;
	}
	@Override
	public int getID() {
		return 5;
	}
}
