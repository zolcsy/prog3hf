package sakk;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import sakk.IJatek;
import sakk.Babu.Szin;

/**
 * Szerializálható Játék osztály, a sakkjátszma valós értékei innen kerülnek kiszámításra, más osztályokban ebből származtatott értékeket kapunk
 * @author karsazoltan
 *
 */
public class Jatek implements IJatek, Serializable {
	/**
	 * a játékhoz tartozó sakktábla
	 */
	private Tabla tabla;
	/**
	 * az utoljára kiválasztott mező
	 */
	private Mezo lastpick;
	/**
	 * ha lehet cserélni
	 */
	private boolean cserevan;
	/**
	 * ha vége a játéknak
	 */
	private boolean nyert;
	/**
	 * a nyerő játékos azonosítója
	 */
	private Szin nyero;
	@Override
	public boolean isNyert() {
		return nyert;
	}
	@Override
	public Szin getNyero() {
		return nyero;
	}
	@Override
	public Szin kiLep() {
		return tabla.kiLep();
	}
	/**
	 * a fehér játékos neve
	 */
	private String feherJatekos;
	@Override
	public String getFeher() {
		return feherJatekos;
	}
	/**
	 * a fekete játékos neve
	 */
	private String feketeJatekos;
	@Override
	public String getFekete(){
		return feketeJatekos;
	}
	/**
	 * Létrehozza a játékot a megadott játékosnevekkel
	 * @param j1 fehér játékos neve
	 * @param j2 fekete játékos neve
	 */
	public Jatek(String j1, String j2) {
		feherJatekos = j1;
		feketeJatekos = j2;
		tabla = new Tabla();
		cserevan = false;
	}
	/**
	 * alapértelmezett konstruktor "feher" és "fekete" játékosnevekkel
	 */
	public Jatek() {
		this("feher", "fekete");
	}
	@Override
	public Mezo getMezo(int i, int j) {
		return tabla.getMezo(i, j);
	}
	@Override
	public Babu getBabu(int x, int y) {
		return tabla.getBabu(x, y);
	}
	@Override
	public boolean pick(int x, int y) throws SzabalytalanLepes {
		Mezo m = tabla.getMezo(x, y);
		if (!m.jo) {
			tabla.rstTable();
			lastpick = m;
			Babu b = m.getBabu();
			if (b != null) {
				if(b.getSzin() != kiLep())
					throw new SzabalytalanLepes("Nem te jössz! A " + kiLep() + " játékos következik");
				if(tabla.getSakk()) {
					List<Mezo> jok = tabla.getLephetokSakkban();
					if(tabla.Sakkmatt()) {
						nyert = true;
						nyero = kiLep() == Szin.feher ? Szin.fekete : Szin.feher; 
					} else if(jok.indexOf(m) != -1) {
						tabla.sakkFeloldok();
						b.HovaLephetekSakkban(m, tabla);
					} 
				}
				else
					b.HovaLephetek(m, tabla);
			} else throw new SzabalytalanLepes("Ott nincs bábú!");
		} else {
			//ekkor megnézzük léphetünk-e oda, ahova, ha igen, el is végezzük
			boolean e = tabla.Lepes(m, lastpick);
			if(tabla.CsereVan() != null) {
				cserevan = true;
			}
			else {
				cserevan = false;
			}
			return e;
		}
		return false;
	}
	@Override
	public int[][] valtozas(){
		return tabla.valtozas();
	}
	@Override
	public Babu CsereKezel(Babu b) {
		//ha lekezeli, feltehetjük hogy ok, megcserélte
		cserevan = false;
		return tabla.cserelParaszt(b);
	}
	@Override
	public Babu[] CsereVan(Szin sz) {
		if(!cserevan || tabla.CsereVan().getBabu().szin != sz) {
			return null;
		}
		return tabla.Cserek(sz);
	}
	/**
	 * A programnak megfelelő .sakk formátumba menti a szerializálható osztályt
	 * @param fn a fájl neve
	 * @param j a szerializálni kívánt Jatek egy példánya
	 */
	public static void Ment(String fn, Jatek j) {
		try {
			FileOutputStream fos = new FileOutputStream(fn + ".sakk");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(j);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Betölti a lementett .sakk formátumú fájlból a Jatek példányt
	 * @param fn a fájl teljes elérési útja és neve
	 * @return a betöltött Jatek példány, ha sikerül
	 */
	public static Jatek Betolt(String fn) {
		try {
			FileInputStream fis = new FileInputStream(fn);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Jatek j = (Jatek) ois.readObject();
			ois.close();
			return j;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public Jatek getJatek() {
		return this;
	}
	@Override
	public boolean getOnline() {
		return false;
	}
	@Override
	public Babu leveheto() {
		return tabla.leveheto();
	}
	@Override
	public Eredmeny getEredmeny() {
		if(nyert) {
			return new Eredmeny(feherJatekos, feketeJatekos, nyero == Szin.feher ? feherJatekos : feketeJatekos, getOnline());
		}
		return new Eredmeny(feherJatekos, feketeJatekos, "?", getOnline());
	}
	
}
