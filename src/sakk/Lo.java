package sakk;

public class Lo extends Babu {
	public Lo(Szin s) {
		super(s);
		lep = new Lepesek(new int[] {1, 2} , new int[] {2, 1});
	}
	public Lo(Lo l) {
		this(l.szin);
	}
	@Override
	public boolean rekurziv() {
		return false;
	}
	@Override
	public boolean forgathato() {
		return true;
	}
	@Override
	public boolean isKiraly() {
		return false;
	}
	@Override
	public int getID() {
		return 4;
	}
}
