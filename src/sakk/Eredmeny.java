package sakk;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Eredmények tárolására szolgáló osztály
 * @author karsazoltan
 *
 */
public class Eredmeny {
	/**
	 * a fehér játékos neve
	 */
	private String feher;
	/**
	 * a fekete játékos neve
	 */
	private String fekete;
	/**
	 * ki nyert
	 */
	private String nyert;
	
	/**
	 * a játék online volt-e
	 */
	private boolean online;
	/**
	 * mikor lett vége a játéknak
	 */
	private LocalDate ido;
	/**
	 * konstruktor
	 * @param feh fehér név
	 * @param fek fekete név
	 * @param ny  ki nyert
	 * @param onlinegame online
	 */
	public Eredmeny(String feh, String fek, String ny, boolean onlinegame) {
		this(feh, fek, ny, onlinegame, LocalDate.now());
	}
	/**
	 * konstruktor
	 * @param feh fehér név
	 * @param fek fekete név
	 * @param ny  ki nyert
	 * @param onlinegame online
	 * @param d adott idő
	 */
	public Eredmeny(String feh, String fek, String ny, boolean onlinegame, LocalDate d) {
		feher = feh;
		fekete = fek;
		nyert = ny;
		online = onlinegame;
		ido = d;
	}
	public String toString() {
		return feher + " " + fekete + " " + ido.format(DateTimeFormatter.ofPattern("yyyy.MM.dd."));
	}
	public String getFeher() {
		return feher;
	}
	public String getFekete() {
		return fekete;
	}
	public String getNyert() {
		return nyert;
	}
	public boolean isOnline() {
		return online;
	}
	public LocalDate getIdo() {
		return ido;
	}
}
