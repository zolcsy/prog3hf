package main;

import java.io.IOException;
import java.util.List;

import org.jdom2.JDOMException;

import gui.Menu;
import gui.TablaPanel;
import gui.sakkGUI;

import sakk.EredmenyekXML;
import sakk.EredmenyTest;

public class Main {

	public static void main(String[] args) {
		Menu m = new Menu();
		TablaPanel tp = new TablaPanel();
		sakkGUI sg = new sakkGUI(m, tp);
		tp.setListeners(sg);
		m.setListeners(sg, tp);
		sg.visible();
	}
}
