# A programról

A játékprogram a *Programozás alapjai 3.* tárgy házifeladata, készítette: *Karsa Zoltán István* (2019/ősz). Az érdeklődők számára egy kezdetleges **Java RMI** (Remote Method Invocation) megvalósítás is szerepel a programban, az online játékhoz, így magas szinten kezelhetők a hálózati kapcsolatok TCP alapon.

## Játék a porgrammal

A jéték fajtáját alapvetően két csoportra bonhatjuk: offline és online játékra. Offline játék esetén a játékosok egy gépet használnak és felváltva lépnek, míg online játék esetén lehetőség van távoli gépek közötti játszmák lebonyolítására!

## Offline Játék megkezdése

Inditás után a fenti menüből a Játék/Új játék lehetőséget kiválasztva, majd a játékosok neveinek megadása után indul a játék!

Miden esetben a felső területen elhelyezkedő fehér játékos kezd. Azon bábúra kattint, amellyel lépni szeretne, azután a zöld mezők közül választva - amik a szabályos lépéssel elérhető mezők - a kívánt mezőre lép a bábú, majd az ellenfél is lép hasonló módon.

Sakk esetén a játék szintén csak oda enged lépni, amivel a sakk elkerülhető, vagy ha nincs ilyen akkor közli a sakkmatt fennállását és felajaánlja a lehetőséget az eredmény elmentésére!

Ha valamelyik paraszt bábunkkal beértünk és van cserebábú a felugró ablak listájából könnyedén választhatunk.

## Online Játék

Online játék esetén a játékosok közül valakinek el kell indítania egy szervert a Játék/Új sakkszerver menüponttal. A szervert nyújtó játékos gépén a következő beállításokat kell elvégezni: a tűzfal lekapcsolása és a portok engedélyezése, máskülönben nem tud a távoli játékos csatlakozni. a másik játékos Játék/Csatlakozás online játékhoz menüponttal léphet be.

A játék hasonló módon kezelhető, mint offline esetben. Online játék esetén amíg a vezérlés nem nálunk van, vagyis nem mi lépünk, a tábblát letiltja a program, elszürkül a játéktér. Miután ellenfelünk lépett a játéktér megváltozása után mi következünk és léphetünk.

## Játékállás mentése és betöltése

Bármelyik módban a játékálás lementhető a Játék/Mentés menüponttal, online játék esetén ha kliensként kívánjuk a játékot lementeni a hálózat miatt ez egy kicsit tovább is tarthat.

Betöltésre két lehetőségünk van: a Játék/Betöltés menüponttal a lementett játékállás kerül folytatásra offline módban, míg a Játék/Új sakkszerver mentett játékkal lehetőséggel a mentett játékot tudjuk online folytatni, ekkor értelemszerűen azon az oldalon nyílik a szerver ahol ezt az opciót választottuk, a másik játékos a szokásos Játék/Csatlakozás online játékhoz opcióval léphet be. Mindkét esetben ha avan egy megkezdett játszma az eldobásra kerül!

## Eredmények megtekintése

A Egyéb/Eredmények opcióval a mentett eredmények tekinthetők meg: ellenfelek, ki nyert, milyen mód volt és mikor. A lenti keresőben megadhatunk játékosneveket, ha van találat, a listában az adott rekod(ok) háttere zöld illetve piros lesz, attól függően hogy a keresett játékos nyert illetve vesztett.