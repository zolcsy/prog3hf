package sakk;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class jatekTest {
	Jatek j;
	@Before
	public void rstJatek() {
		j = new Jatek();
	}
	@Test(expected=SzabalytalanLepes.class)
	public void nincsBabu() throws SzabalytalanLepes {
		j.pick(4, 4);
	}
	@Test(expected=SzabalytalanLepes.class)
	public void Rosszlepo() throws SzabalytalanLepes {
		j.pick(7, 1);
	}
	@Test
	public void LepLo() throws SzabalytalanLepes {
		j.pick(0, 1); //ló
		j.pick(2, 0);
		assertEquals("sikerült ellépni", j.getMezo(0, 1).ures(), true);
		assertEquals("lóval léptünk", j.getBabu(2, 0).getID(), 4);
	}
	@Test
	public void LepParaszt() throws SzabalytalanLepes {
		j.pick(1, 1); //paraszt
		j.pick(3, 1);
		assertEquals("sikerült ellépni", j.getMezo(1, 1).ures(), true);
		assertEquals("paraszttal léptünk", j.getBabu(3, 1).getID(), 6);
	}
	@Test(expected=SzabalytalanLepes.class)
	public void LepNemtud() throws SzabalytalanLepes {
		j.pick(0, 4);
		j.pick(3, 4);
	}
	@Test
	public void MentesBetoltes() throws SzabalytalanLepes {
		j.pick(0, 1); //ellépünk
		j.pick(2, 0);
		Jatek.Ment("test", j);
		Jatek betolt = Jatek.Betolt("test.sakk");
		assertEquals("Jó beolvasás", betolt.getBabu(2, 0).getID(), j.getBabu(2, 0).getID());
	}
	@Test(expected=SzabalytalanLepes.class)
	public void SakkbanLepesRosszal() throws SzabalytalanLepes {
		sakk(); //sakk helyzet előidézése
		//királlyal elpróbálunk lépni, de nemtudunk
		j.pick(0 ,4 );
		j.pick(1 ,4 );
		
	}
	@Test
	public void SakkbanLepesJoval() throws SzabalytalanLepes {
		j.pick(0 ,6 );
		j.pick(1 ,4 );
	}
	public void sakk() throws SzabalytalanLepes {
		//sakkhelyzet előidézése:
		j.pick(1 ,4 );
		j.pick(3 ,4 );
		j.pick(6 ,5 );
		j.pick(4 ,5 );
		j.pick(3 ,4 );
		j.pick(4 ,5 );
		j.pick(6 ,4 );
		j.pick(5 ,4 );
		j.pick(4 ,5 );
		j.pick(5 ,4 );
		j.pick(7 ,4 );
		j.pick(5 ,4 );
	}
}
