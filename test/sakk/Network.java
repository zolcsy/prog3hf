package sakk;

import static org.junit.Assert.*;

import java.rmi.RemoteException;

import org.junit.Before;
import org.junit.Test;

import sakk.Babu.Szin;

public class Network {
	SakkServer ss;
	SakkClient sc1;
	SakkClient sc2;
	
	@Before
	public void init() throws RemoteException {
		ss = new SakkServer("feher", "fekete");
		ss.start();
		sc1 = new SakkClient("localhost", Szin.feher);
		sc2 = new SakkClient("localhost", Szin.fekete);
	}
	@Test
	public void Ellenoriz() throws RemoteException {
		assertEquals(sc1.getEllenfelOK(), true);
		assertEquals(sc2.getEllenfelOK(), true);
	}
	@Test
	public void lepes() throws RemoteException, SzabalytalanLepes {
		Babu b = sc1.getBabu(1, 3);
		sc1.pick(1, 3);
		sc1.pick(3, 3);
		//a szerver kérdezése:
		assertEquals(ss.getBabu(3, 3), b);
		//változott a helyzet?
		assertEquals(sc2.tavoliValtozas(), true);
	}
}
