package sakk;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import org.jdom2.JDOMException;
import org.junit.Before;
import org.junit.Test;

import org.jdom2.*;

public class EredmenyTest {
	EredmenyekXML exml;
	@Before
	public void init() {
		exml = new EredmenyekXML();
	}
	@Test
	public void EredmenyTest() throws Exception {
		List<Eredmeny> list =  exml.EredmenyOlvas();
		boolean ok = false;
		for(Eredmeny e : list) {
			if(e.getFeher().compareTo("feherTEST") == 0) {
				if(e.getFekete().compareTo("feketeTEST") == 0) {
					if(e.getNyert().compareTo("feherTEST") == 0)
						ok = true;
				}
			}
		}
		assertEquals(ok, true);
	}
}
